from bisect import bisect_right
from typing import List, TypeVar
from Encoder import Encoder

import matplotlib.pyplot as plt
import numpy as np

T = TypeVar('T', bound='LineEncoder')  # type definition for factory methods


class LineEncoder(Encoder):
    """
    This class encodes a waveform (i.e., a sequence of sampling points)in a space-efficient manner. It is based on the
    assumption that most waveform points lie on straight line segments. Therefore, only points where two segments
    intersect, i.e., where the slope of the waveform changes, are relevant and need to be saved. These points are called
    'transition points'. All other points on a segment can be calculated from the waveform value at the start of the
    segment and its slope.
    If the increments between adjacent points are non-constant, we treat each such point as a segment consisting of
    only that point.

    The waveform is therefore encoded in two lists, t and y, that hold the horizontal (time) and vertical (waveform
    value) coordinate of the transition points.

    Lookup of a waveform value, f, at a given time t is then simple:
    1) Convert t to the index i of the sampling point
    2) Find the last transition point <= i
    3) Calculate the value of f based on the waveform value at i and the slope of the segment beginning at i.

    Example:
        index:          [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        waveform:       [0, 1, 2, 3, 4, 4, 4, 4, 3, 2,  1,  0]

           *___*
           /   \
          /     \
         /       \      * = transition point
        /         \
        *         *

        t:              [0, 4, 7, 11]   # horizontal coordinate of transition points
        y:              [0, 4, 4,  0]   # vertical coordinate



    To avoid rounding errors, all function values are treated as integers. When importing data, it has to be ensured
    that the waveform is integer-based. The class member 'scaling_factor' can be used to scale integers to arbitrary
    real-world floating point values.

    If the waveform does not start at t=0, it is mandatory that the first point stored in the (t, y) vectors is (0,
    0). This point will automatically be inserted if the waveform starts with a point t>0

    To instantiate an object, factory methods exist for generating the object from a value vector, a difference
    vector (as produced by numpy.diff), and an runlength encoded (rle) difference vector. The latter has uses the
    following convention: if a difference value t appears more than once in a row, it is encoded as
    't, t, count', where count specifies how many more times the value t has to be repeated. E.g., six consecutive
    values 3 would be encoded as 3, 3, 4, whereas only 2 occurrences would be encoded as 3, 3, 0.
    All factory methods use the import for rle-encoded vectors, since this usually requires less memory than expanding
    the waveform into a fully sampled vector. The sequence to import a waveform is therefore
    waveform -> difference vector -> runlength encoding -> import via (add_value).
    This is handled internally by the factory methods.

    TODO: - vector t has been renamed t, but there might be references in the code to t. Remove these - Scaling
    factor might not be needed, and makes things more complicated, e.g., when adding waveforms with different scaling
    factors. Maybe store the actual values of y rather than just integers that need to be scaled.
    """

    length: int = 0  # the number of points in the fully samples waveform
    scaling_factor: float = 1.0  # a multiplicative scaling factor for y values
    dt_us: int = 10
    t: List[int] = []
    y: List[int] = []

    def __init__(self, dt_us: int = 10, scaling_factor: float = 1.0, t: List[int] = None, y: List[int] = None):
        """
        The constructor. Initialize an empty encoder, or add points if t and y are specified.
        :param dt_us: the temporal spacing between sample points, in µs.
        :param scaling_factor: the multiplicative scaling factor to scale the integer representation to real-world
        :param t: list of t coordinates of transition points
        :param y: list of y coordinates of transition points. Either both or neither of t, y have to be specified.
        """

        super().__init__()
        self.dt_us = dt_us
        self.scaling_factor = scaling_factor

        if (t is None and y is not None) or (t is not None and y is None):
            raise TypeError('Either both or none of the optional parameters t and y have to be specified')

        # coordinates of transition points
        if t is not None:  # in this case, y is not None as well, thanks to the above check
            if isinstance(t, np.ndarray):
                self.t = t.tolist()
            else:
                self.t = t
            if isinstance(y, np.ndarray):
                self.y = y.tolist()
            else:
                self.y = y

            # if the first point is not zero, we set it to zero and complain about it:
            if self.y[0] != 0:
                self.y[0] = 0

            if len(self.y) > 1 and self.y[-1] != 0:
                self.t += [self.t[-1] + self.dt_us]
                self.y += [0]

            self.length = int(round(self.t[-1] - t[0]) / self.dt_us) + 1
        else:
            self.t = []
            self.y = []
            self.length = 0
        # otherwise, t, y and length are initialized with default values

    def add_point(self, t, y):

        if len(self.t) == 0 and t != 0:  # make sure that the first point is always (0, 0)
            self.t += [0]
            self.y += [0]
        self.t += [t]
        self.y += [y]
        self.length = int(t / self.dt_us)

    def get_number_of_points(self) -> int:
        """
        get the number of data points in the fully sampled waveform
        :return: the number of points
        """
        return self.length

    def get_total_time(self) -> int:
        """
        Ge the total duration of the waveform
        :return: the total duration, in µs
        """
        if len(self.t) > 0:
            return self.t[-1]
        else:
            return 0

    def find_start_index_of_segment(self, t: int) -> int:
        """
        Find the start index of the segment that contains time point t using binary search for performance
        :param t: time (in µs)
        :return: the index of the start of the segment containing time point t
        """

        if t < 0:
            print('Error: querying time point < 0: ' + str(t))
            return 0

        if t > self.get_total_time():
            return len(self.t) - 1

        #  # the absolute index into the waveform corresponding to time t
        x = bisect_right(self.t, t)

        # If a_ind is in self.t, t is by one larger than the required index
        # If a_ind is not in self.t, t is also the required index +1
        x -= 1
        return x

    def get_value_at_time(self, t: int) -> float:
        """
        Get the value of the waveform at time t
        :param t: timepoint in µs
        :return: the value of the waveform at time t
        """
        #a_ind = round(t / self.dt_us)

        q = self.find_start_index_of_segment(t)

        if t == self.t[q]:  # a_ind points to an intersection point, so we know the waveform value already
            return self.y[q] * self.scaling_factor

        # otherwise, we have to calculate the function value from the previous transition point and the slope of
        # the line segment:

        if q < len(self.t) - 1:  # the previous intersection point is not the last point of the waveform
            slope = self.get_slope(q)
            return (self.y[q] + (t - self.t[q]) * slope) * self.scaling_factor

        else:  # q is the last transition point -> return that value
            return self.y[q] * self.scaling_factor

    def get_slope(self, k) -> float:
        """
        Get the slope of the k-th segment. Slope is always an integer, since only integer increments are allowed when
        importing the waveform.
        :param k: The 0-based index of the segment
        :return: the slope
        """

        if len(self.t) < 2:  # there is no slope defined
            return 0

        if k < len(self.t) - 1:
            return (self.y[k + 1] - self.y[k]) / (self.t[k + 1] - self.t[k])
        elif k == len(self.t) - 1:
            return (self.y[k] - self.y[k - 1]) / (self.t[k] - self.t[k - 1])

        else:  # after the last intersection point, we assume a slope of 0:
            return 0

    def expand_fully(self, start_time: int = 0, end_time: int = -1) -> (np.ndarray, np.ndarray):
        """
        Expand the waveform (see description of expand(...), but do not use compression of zero segments.
        :param start_time: start time of the interval to be expanded (optional)
        :param end_time: end time of the interval to be expanded
        :return: a tuple of t and y coordinate lists of the sample points
        """
        return self.expand(start_time, end_time, compress_zeros=False)

    def expand(self, start_time: int = 0, end_time: int = -1, compress_zeros: bool = True) -> (np.ndarray, np.ndarray):
        """
        Expand the waveform into a pair of lists t, y of sample points with sampling rate self.dt_us. If compress_zeros
        = True (default setting), intervals where y=0 are skipped, i.e. only the first and last point of such an
        interval are stored. If the start and end time of the sampling interval are not specified, the entire waveform
        is expanded.
        If the encoder does not encode any points, a single point (0, 0) will be returned
        :param start_time: start time of the interval to be expanded (optional)
        :param end_time: end time of the interval to be expanded
        :param compress_zeros: if true, segments where y=0 are compressed by only storing the first and last point of
        the section.
        :return: a tuple of t and y coordinate lists of the sample points
        """
        if len(self.t) == 0:  # return a single point if the encoder is empty
            return np.asarray([0]), np.asarray([0])

        tt, wf = self.t, self.y

        # step 1: produce list of line segments of the type (t0, y0, t1, y1):
        segments = [(tt[i], wf[i], tt[i+1], wf[i+1]) for i in range(0, len(tt)-1)]

        # step 2: define a function that expands a segment into a tuple (t_arr, y_arr)
        def expand_segment(t0, y0, t1, y1) -> (np.ndarray, np.ndarray):

            y_thresh = 1e-5
            dt_us = self.dt_us  # time resolution

            if np.abs(y1-y0) < y_thresh and np.abs(y0) < y_thresh and compress_zeros:
                # for a zero segment, we only need to add one point (the start of the segment. The end point will be
                # the start point of the following non-zero segment.
                t_arr = np.asarray([t0])
                y_arr = np.asarray([y0])

            else:
                t_arr = np.arange(t0, t1, dt_us)
                n = len(t_arr)
                y_arr = np.arange(0, n)/n * (y1-y0) + y0

            return t_arr, y_arr

        # step 3: expand each segment using the expansion function:
        expanded_segments = [expand_segment(q[0], q[1], q[2], q[3]) for q in segments]
        # expanded_segments is a list of the form ((t_arr, y_arr), (t_arr, y_arr), ...)

        # step 4 sort the segments list into two lists
        t = [x[0] for x in expanded_segments]
        y = [x[1] for x in expanded_segments]

        t = np.concatenate(t).ravel()
        y = np.concatenate(y).ravel()

        # now we must not forget to add the last point:
        t = np.append(t, tt[-1])
        y = np.append(y, wf[-1])

        return t, y

    def __add__(self, other: 'LineEncoder') -> 'LineEncoder':

        if self.dt_us != other.dt_us:
            raise ValueError('Both LineEncoders must have the same time increments dt_us (' + str(self.dt_us) + ' vs. ' +
                             str(other.dt_us) + ')')

        if self.scaling_factor != other.scaling_factor:
            raise ValueError('Both LineEncoders must have the same scaling_factor (' + str(self.scaling_factor) +
                             ' vs. ' + str(other.scaling_factor) + ')')

        enc = LineEncoder(self.dt_us, scaling_factor=self.scaling_factor)

        # deal with the case that one encoder is empty:
        if len(self.t) == 0:
            enc.t += other.t
            enc.y += other.y
            return enc
        elif len(other.t) == 0:
            enc.t += self.t
            enc.y += self.y
            return enc

        # one common scenario (esp. for RF pulses) is adding two non-overlapping waveforms, in this case we can basically
        # just concatenate them (with few modifications to the second waveform), which is much faster than adding them
        # point by point. We check if this is applicable here.

        if len(self.y) > 1 and self.y[1] == 0:  # meaning that self does not start with a slope right away, but has a zero line from t[0] to t[1]
            if self.t[1] >= other.t[-1]:  # if this is true, other ends before self begins -> yay!
                enc.t += other.t
                enc.y += other.y
                if self.t[1] == other.t[-1]:  # self and other share one point, only add it once
                    enc.t += self.t[2:]
                    enc.y += self.y[2:]
                else:
                    enc.t += self.t[1:]
                    enc.y += self.y[1:]
                return enc
        if len(other.y) > 1 and other.y[1] == 0:  # meaning that other does not start with a rising slope
            if other.t[1] >= self.t[-1]:  # other begins after self has ended -> perfect!
                enc.t += self.t
                enc.y += self.y
                if self.t[-1] == other.t[1]:
                    enc.t += other.t[2:]
                    enc.y += other.y[2:]
                else:
                    enc.t += other.t[1:]
                    enc.y += other.y[1:]
                return enc
        # otherwise, we have to combine the two waveforms point by point
        c1 = 0
        c2 = 0

        while c1 < len(self.t) and c2 < len(other.t):
            if self.t[c1] == other.t[c2]:
                val = self.y[c1] + other.y[c2]
                enc.add_point(self.t[c1], val)
                c1 += 1
                c2 += 1

            elif self.t[c1] < other.t[c2]:
                t = self.t[c1]
                val = self.y[c1] + other.get_value_at_time(t)
                enc.add_point(t, val)
                c1 += 1
            else:  # self.t[c1] > other.t[c2
                t = other.t[c2]
                val = other.y[c2] + self.get_value_at_time(t)
                enc.add_point(t, val)
                c2 += 1

        # process any remaining points
        while c1 < len(self.t):
            enc.add_point(self.t[c1], self.y[c1])
            c1 += 1
        while c2 < len(other.t):
            enc.add_point(other.t[c2], other.y[c2])
            c2 += 1

        return enc

    def __iadd__(self, other: 'LineEncoder'):
        enc = self + other
        self.t = enc.t
        self.y = enc.y
        self.length = enc.length

    def get_waveform(self, start_time: int = 0, end_time: int = -1) -> (np.ndarray, np.ndarray):
        """
        Calculate a compressed version of the waveform as lists containing the coordinates of the transition
        points. The resulting lists can be used as the input to matplotlib's plot command, i.e.:

        t, v = enc.get_waveform(0, 1000)
        matplotlib.pyplot.plot(t, v)

        :param start_time: start time in µs (default: 0)
        :param end_time: end_time in µs (inclusive) (default: end of waveform)
        :return: t, values - two 1D arrays, holding time and value for the slope transition points.
        """

        if len(self.t) == 0:
            return np.asarray([0]), np.asarray([0])

        if end_time == -1:
            end_time = self.get_total_time()

        x0 = self.find_start_index_of_segment(start_time)
        x1 = self.find_start_index_of_segment(end_time)

        tt: np.ndarray = np.asarray(self.t[x0:(x1 + 1)])
        yy: np.ndarray = np.asarray(self.y[x0:(x1 + 1)]).astype(float)
        # x0 points to the last transition point <= start_time. We therefore might have to change the t and y
        # coordinates of the first point
        offset = start_time - tt[0]
        tt[0] += offset
        yy[0] += offset * self.get_slope(x0)

        # if the last point of the waveform is not a transition point, we have to interpolate the final value
        if end_time - self.t[x1] > 0:
            tt = np.append(tt, end_time)
            m = self.get_slope(x1)
            dt = end_time - self.t[x1]
            yy = np.append(yy, yy[-1]+m*dt)

        # scale the output data to µs and real-world values
        yy *= self.scaling_factor

        return tt, yy

    def plot_waveform(self):
        t, y = self.get_waveform()
        plt.subplots()
        plt.plot(t, y)

    def set_start_time(self, start_time: int) -> None:
        """
        Set the start time of the first slope
        :param start_time: the start time of the first slope
        :return: nothing
        """

        if self.t is None or len(self.t) == 0:
            return  # nothing to do if there are no points

        # if there is only one point (for whatever reason), we shift that point
        if len(self.t) == 1:
            self.t[0] += start_time

        # if self.t is not empty, we expect at least three points, since a delta peak would be encoded as three points
        # (with leading and trailing zero points)

        # the first point is always expected to be (0, 0), so we have to check whether the second point is already the
        # end of the rising slope, or its beginning

        if self.y[1] != 0:  # the waveform begins with a rising slope -> move the entire waveform, and prepend a zero
            # point to the beginning
            self.t = [x + start_time for x in self.t]
            self.t = [0] + self.t
            self.y = [0] + self.y
        else:  # the waveform begins with a horizontal slope, so we only need to modify the time points and keep the
            # (0, 0) point
            for j in range(1, len(self.t)):
                self.t[j] += start_time

    def __mul__(self, other) -> 'LineEncoder':
        """
        Multiply (scale) the waveform with a scalar factor
        :param other: the multiplicative scaling factor. Has to be of type int or float
        :return: a copy of the LineEncoder with the amplitude scaled
        """
        if not isinstance(other, (float, int)):
            raise ValueError(f'Multiplication of LineEncoder and incompatible type {type(other)}. Only int and float are permitted.')

        t = self.t.copy()
        y = [v*other for v in self.y]

        enc = LineEncoder(self.dt_us, self.scaling_factor, t, y)
        return enc

    def __rmul__(self, other) -> 'LineEncoder':
        """
        Right multiplication with a scaling parameter x*self
        :param other: a scalar int or double parameter
        :return: A copy of self, with the amplitude scaled
        """
        return self.__mul__(other)

    @classmethod
    def run_length_encode(cls, diffs: list) -> list:
        """
        run-length encode the *difference* value vector (created by numpy.diff(waveform)). See documentation at
        the beginning of this file for a documentation of the rle format.
        :param diffs: a 1D numpy array of first-order differences
        :return: a run-length encoded representation of the difference vector
        """
        last_val = None
        counter = 0
        encoded: List[int] = []

        for x in diffs:
            if x != last_val:  # the current value is different from the previous value

                if counter > 0:  # write accumulated copies of previous value
                    if counter == 1:
                        encoded += [last_val, 0]  # this means that the value occurred exactly twice,
                        # so zero more _repetitions
                    else:
                        encoded += [last_val, counter - 1]
                encoded += [x]
                last_val = x
                counter = 0
            else:  # t == last_val
                counter += 1

        # if counter > 0, we still need to process the accumulated copies of the last vector
        if counter > 0:  # write accumulated copies of previous value
            if counter == 1:
                encoded += [last_val, 0]  # this means that the value occurred exactly twice, so zero more _repetitions
            else:
                encoded += [last_val, counter - 1]

        return encoded

    @classmethod
    def from_differences(cls, diffs: np.ndarray, dt_us: int,
                         scaling_factor: float = 1.0) -> T:  # T is a TypeVar pointing towards LineEncoder
        """
        A factory method to create a LineEncoder from a non-encoded difference vector.
        :param diffs: a 1D numpy array holding the difference values, e.g., as created by numpy.diff(waveform)
        :param dt_us: the sampling interval, in µs
        :param scaling_factor: a multiplicative scaling factor for the waveform, i.e. output=values*scaling_factor
        :return: a DifferentialEncoder object
        """
        rle = cls.run_length_encode(diffs)
        enc = cls.from_rle_encoded_differences(rle, dt_us, scaling_factor)
        return enc

    @classmethod
    def from_rle_encoded_differences(cls, diffs_enc: list, dt_us: int, scaling_factor: float = 1.0) -> T:
        """
        A factory method to create a LineEncoder from a run-length encoded difference vector
        :param diffs_enc: a 1D numpy array holding the rle-encoded differerences
        :param dt_us: the sampling interval, in µs
        :param scaling_factor: a multiplicative scaling factor for the waveform, i.e. output=values*scaling_factor
        :return: a LineEncoder object
        """

        x, y = cls.rle_to_points(diffs_enc, dt_us)

        enc = LineEncoder(dt_us, scaling_factor, x, y)

        return enc

    @classmethod
    def from_waveform(cls, waveform: list, dt_us: int, scaling_factor: float = 1.0) -> T:
        """
        A factory method to create a LineEncoder from a waveform vector (i.e. function values, not differences)
        :param waveform: a 1D numpy array holding the waveform (function) values
        :param dt_us: the sampling interval, in µs
        :param scaling_factor: a multiplicative scaling factor for the waveform, i.e. output=values*scaling_factor
        :return: a LineEncoder object
        """
        t = list(range(0, len(waveform)*dt_us, dt_us))
        x, y = cls.waveform_to_points(t, waveform, dt_us)
        enc = LineEncoder(dt_us, scaling_factor, x, y)
        return enc

    @classmethod
    def rle_to_points(cls, rle: list, dt_us: int = 10) -> (list, list):

        eps = 1.0e-6  # tolerance

        segments = []  # the list of encoded segments. Each entry is a tuple of the form (value,
        # number_of_occurences), i.e., (1, 3) means three consecutive 1's

        i = 0
        while i < len(rle):
            if i == len(rle)-1:  # the last segment, if it is a single (non-repeated) point
                segments += [(rle[i], 1)]
                i += 1
            elif rle[i] == rle[i + 1]:
                segments += [(rle[i], rle[i + 2] + 2)]
                i += 3
            else:
                segments += [(rle[i], 1)]
                i += 1
        if i < len(rle) - 1:
            segments += [(rle[-1], 1)]

        # the waveform always starts with zero
        x = [0]
        y = [0]

        t = 0  # time point counter
        c = 0  # current value
        for s in segments:
            c += s[0] * s[1]
            t += s[1]
            x += [t]
            y += [c]

        # add zero at the end, if last point is not equal to zero
        if abs(y[-1]) < eps:  # if the last point is (almost) zero, we make it zero
            y[-1] = 0
        else:  # otherwise, we add a zero point after the last encoded point
            y += [0]
            x += [x[-1] + 1]

        x = [a*dt_us for a in x]

        return x, y

    @classmethod
    def waveform_to_points(cls, t: list, y: list, dt_us: int = 10) -> (list, list):

        eps = 1.0e-6  # the epsilon for comparing slopes

        if len(t) != len(y):
            raise ValueError('Arrays t and y need to have the same size! (' + str(len(t)) + ' vs . ' + str(len(y)) + ')')

        # if there is only one point in the waveform, we add 0 before and after it, leading to a sharp triangle
        if len(t) == 1:
            if y[0] == 0:
                return t, y
            else:
                tt = np.asarray([t[0] - dt_us, t[0], t[0] + dt_us])
                yy = np.asarray([0, y[0], 0])
                return tt, yy

        # we filter the data points and only keep those where the slope changes
        keep_points = [0]

        for i in range(1, len(t) - 1):
            forward_diff = (y[i + 1] - y[i]) / (t[i + 1] - t[i])
            backward_diff = (y[i] - y[i - 1]) / (t[i] - t[i - 1])

            if abs(forward_diff - backward_diff) > eps:
                keep_points += [i]

        tt = [t[j] for j in keep_points]
        yy = [y[j] for j in keep_points]

        # if the waveform does not start with zero, we add one point before the start:
        if abs(yy[0]) > eps:
            yy = [0] + yy
            tt = [t[0] - dt_us] + tt

        # the last point is never part of tt and yy, we need to add it manually
        tt += [t[-1]]
        if abs(y[-1]) < eps:  # if the last part is almost zero, we make it zero
            yy += [0]
        else:  # we add the last point as it is and then a zero point after it
            tt += [tt[-1] + dt_us]
            yy += [y[-1], 0]

        return tt, yy


def main():
    from SequenceTiming import SequenceTiming
    from SequenceDiagram import SequenceDiagram
    st = SequenceTiming('examples/FLASH/SimulationProtocol_INF.dsv')
    s = SequenceDiagram(st, cont_update=True)
    s.plot()
    enc = s.st.wf_grz
    enc.get_waveform(0, 1500)

    print(",\t".join(['%8d' % (x * 10) for x in enc.t[0:10]]))
    print(",\t".join(['%8d' % y for y in enc.y[0:10]]))


def test():
    from Atoms.GradientAtom import GradientAtom
    from Atoms.AtomType import AtomType
    g = GradientAtom(300, 'test', 25.6, 300, 10, 10, AtomType.GR, None)
    enc = g.get_encoder()
    enc2 = LineEncoder(10, 1.0)
    enc3 = enc2 + enc


if __name__ == '__main__':
    # main()
    test()
