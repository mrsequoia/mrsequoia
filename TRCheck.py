"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import matplotlib.pyplot as plt

from CheckBase import CheckBase
from SequenceTiming import SequenceTiming
from Types import SelectorType


class TRCheck(CheckBase):
    """
    TRTest: Verify that TR has a constant value. This is achieved by checking that the timing between atoms that should
    occur once per TR is constant
    """

    def __init__(self, designator: SelectorType, name: str = None, slices: int = 1):
        """
        The constructor
        :param designator: string or function reference Atom -> bool to select an atom that occurs at
        a fixed position once per TR (e.g., an excitation pulse).
        :param name: The name of the test to be used when reporting the result.
        :param slices: the number of slices in the protocol. If specified, TR will be accumulated over <slices>
        occurences, since Siemens specified TR as the acquisition of a complete volume, not a single slice
        """
        if name is None:
            name = 'TRCheck'
        super().__init__(name)
        self.designator: SelectorType = designator
        self.trs: list = []
        self.slices = slices

    def run_check(self, timing: SequenceTiming, expected_tr: int = None) -> bool:
        """
        Perform the test and print the result
        :param timing: a SequenceTiming object of the protocol to be validated.
        :param expected_tr: optional parameter to verify that TR has a specific value. If omitted, the test only checks
        that TR is constant (but has an arbitrary value).
        :return: a bool indicating whether the test was successful
        """

        ev = self.filter_by_designator(self.designator, timing)

        self.trs = []

        indices = range(0, len(ev), self.slices)

        for r in range(0, len(indices)-1):
            a = indices[r]
            b = indices[r+1]
            self.trs.append(ev[b].start_time-ev[a].start_time)

        s = set(self.trs)
        result = False
        if len(s) > 1:
            print('Found ' + str(len(s)) + ' different TRs:')

            # build a list of TR values with format TR: (X), where X is the number of occurences of TR value
            tr_vals = ["%d: (%d)" % (x, self.trs.count(x)) for x in sorted(list(s))]

            print(", ".join(tr_vals))

        else:
            print('Found one value for TR: ' + str(self.trs[0]) + ' µs')
            result = True

            if expected_tr is not None:
                print('Expected TR: ' + str(expected_tr))
                if 0 < expected_tr != self.trs[0]:
                    result = False

        self.print_success_or_fail(result)
        return result

    def print_trs(self) -> None:
        """
        Print the TR values in chronological order.
        :return: nothing
        """
        print(", ".join([str(tr) for tr in self.trs]))

    def plot_trs(self) -> None:
        """
        Plot TR vs. the TR number
        :return: nothing
        """

        _, ax = plt.subplots()

        plt.plot(range(0, len(self.trs)), self.trs)


if __name__ == '__main__':
    # sample usage
    filename = 'SimOutput/FLASH/SimulationProtocol_INF.dsv'
    seq_timing = SequenceTiming(filename)
    test = TRCheck('SRFExcit')
    test.run_check(seq_timing)
