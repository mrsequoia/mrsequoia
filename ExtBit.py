

class ExtBit:

    RESET_K: int = 1
    REVERSE_K: int = 2
    LOCK_K: int = 3
    RELEASE_K: int = 4
    TIMER: int = 5
    IDEAL_SPOILER: int = 6
    RF_REF: int = 7
    EXECUTE_MOTION: int = 8
    RT_RECON: int = 9

    descriptions = {
        RESET_K: 'reset Kx, Ky and Kz to zero',
        REVERSE_K: 'reverse Kx, Ky and Kz',
        LOCK_K: 'buffer current K space location',
        RELEASE_K: 'set K space location to the latest buffered one, used after Plugin_LockK',
        TIMER: 'calculate remaining scan time, display it on the main control console',
        IDEAL_SPOILER: 'dephase transverse magnetization of all the spins, set them to zero',
        RF_REF: 'buffer current rfPhase value and demodulate signal phase according to this value',
        EXECUTE_MOTION: 'trigger object motion',
        RT_RECON: 'trigger real time image reconstruction with currently stored k-space data'
    }

    start: int
    type: int
    anchor: str

    def __init__(self, _start: int, _type: int, _anchor: str = 'start'):
        self.start = _start
        self.type = _type
        self.anchor = _anchor

    def to_xml(self) -> str:

        notes = self.descriptions.get(self.type, '')

        return f'''\t\t<ExtBit DupSpacing="0" Duplicates="1" Ext="{self.type}" Notes="{notes}" Switch="$1'on','off'"
tStart="{self.start / 1e6:.6f}"/>'''
