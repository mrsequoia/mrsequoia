"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from SeqTree.ActionNode import ActionNode
from SeqTree.SequenceMediator import SequenceMediator
from Atoms.GradientAtom import GradientAtom
from Atoms.AtomType import AtomType
from math import pi, ceil
from SeqTree.ParameterizedFunction import ParameterizedFunction
from MRConstants import gamma_bar, gamma_mT_µs
from typing import List, Union


class PhaseEncoderNode(ActionNode):
    """
    A node that encapsulates a phase-encode gradient and provides methods for automatically calculating the right
    phase-encode gradient amplitude. The sampling convention is to start at the top of k-space (max. pos k-number)
    and sample towards negative k-numbers
    """

    def __init__(self, name: str, mediator: SequenceMediator, duration: int, fov_pe: float,
                 rut: int, rdt: int, pe_index_id: Union[str, List[str]] = 'pe_idx', rewinder: bool = False):
        """
        :param name:
        :param mediator:
        :param duration: duration of the gradient, in µs
        :param fov_pe: the FOV in PE direction, in m
        :param rut: ramp-up time, in µs
        :param rdt: ramp-down time, in µs
        :param pe_index_id: a string identifying the PE line counter in the SequenceMediator instance (i.e., the loop
        index of the PE-loop). This is the base string of the index, without _runs appended.
        Alternatively, a list of two such strings can be provided in the case of segmented k-space sampling, where the
        first string identifies the outer and the second string the inner loop counter. The same logic applies as to a
        single string.
        :param rewinder: set to True if this is a rewinder gradient to undo the effect of the encoder gradient. This
        changes the polarity of the gradient.
        """
        atom = GradientAtom(0, name, 0, duration, rut, rdt, AtomType.GP)
        super().__init__(name, mediator, atom)

        self.segmented = True if isinstance(pe_index_id, list) else False  # are we doing segmented phase encoding?

        self.pe_index_id: str = pe_index_id

        #  from here on, we wrap everything in a function that we can then store in a ParameterizedFunction object.
        # This is necessary, since we want the calculations to be performed every time when the node is executed, so
        # that it keeps up-to-date when the number of phase-encode steps is changed after the node has been created.

        def inner_function(pe_info: List[int]) -> float:
            """
            Calculates the gradient amplitude for the current phase-encode step
            :param pe_info: a list with two elements: [current PE index, number of PE steps]
            :return: the gradient amplitude
            """

            n_pe = pe_info[1]

            # calculate the amplitude difference from one k-space line to the next
            T = duration  # duration of gradient, excluding ramp-up
            grad_calc = self.mediator.get_value('grad_calc')

            deltak = 1/fov_pe
            kmin = ceil((n_pe-1)/2) * deltak # The minimum (most extreme negative) k-space line
            mmin = 2*pi*kmin/gamma_mT_µs  # The moment required to reach kmin
            max_ampl = abs(mmin)/T   # maximum gradient amplitude (positive)

            delta_m = abs(mmin)/ceil((n_pe-1)/2)  # distance between k-space lines (positive)
            delta_ampl = delta_m/T  # the corresponding change in the PE gradient amplitude to encode adjacent k-space
            # lines

            self.atom.amplitude = max_ampl

            self.mediator.set_simulation_attribute('FOVPhase', fov_pe)
            self.mediator.set_simulation_attribute('ResPhase', n_pe)

            # convention for k-space: if the number of PE lines is even, the baseline (with zero amplitude) is the first
            # line of the lower half. If n_pe is odd, the baseline is the center line
            polarity = -1 if rewinder else 1

            # we start by encoding the most negative k-space line (kmin), and move towards the most positive line.
            return polarity * (-max_ampl + pe_info[0]*delta_ampl)  # pe_step[0] is the current phase-encode line index

        # the ParameterizedFunction depends on two variables: the current PE step and the number of PE steps.

        pf = ParameterizedFunction(self, inner_function, ['current_pe_index', 'number_of_pe_steps'])
        self.register_dynamic_properties([('amplitude', pf)])

    def get_additional_info(self) -> str:
        info = super().get_additional_info()
        if self.segmented:
            n_pe = self.mediator.get_value(self.pe_index_id[0] + '_runs') * self.mediator.get_value(self.pe_index_id[1] + '_runs')
        else:
            n_pe = self.mediator.get_value(self.pe_index_id + '_runs')
        return f'{info}<BR/>PE-steps: {n_pe}'

    @property
    def current_pe_index(self) -> int:
        """
        Get the current phase-encode index. This has to be implemented as a @property to make it work with the lookup
        mechanism of ParameterizedFunction.
        :return: The current phase-encode index
        """
        if self.segmented:
            current_outer_run = self.mediator.get_value(self.pe_index_id[0] + '_current')
            inner_runs_per_outer_run = self.mediator.get_value(self.pe_index_id[1] + '_runs')
            current_inner_run = self.mediator.get_value(self.pe_index_id[1] + '_current')
            return current_outer_run * inner_runs_per_outer_run + current_inner_run
        else:
            return self.mediator.get_value(self.pe_index_id + '_current')

    @property
    def number_of_pe_steps(self) -> int:
        """
        Get the total number of phase-encode steps. This has to be implemented as a @property to make it work with the
        lookup mechanism of ParameterizedFunction.
        :return: The number of phase-encode steps
        """
        if self.segmented:
            outer_steps = self.mediator.get_value(self.pe_index_id[0] + '_runs')
            inner_steps = self.mediator.get_value(self.pe_index_id[1] + '_runs')
            return inner_steps * outer_steps
        else:
            return self.mediator.get_value(self.pe_index_id + '_runs')


    def rename_loop_indices(self, prefix: str, indices_to_rename=None) -> None:
        """
        Rename the loop indices of all children by appending prefix + "_" to them. This is used when cloning a tree, e.g.,
        to create prepscans.
        :param prefix: a string that will be prepended to each loop index found.
        :param indices_to_rename: a list of all indices found so far. This will be used by all ActionNode instances
        that have to rename their dependencies on dynamic variables.
        :return: nothing
        """

        if indices_to_rename is None:
            indices_to_rename = []

        if self.segmented:
            self.pe_index_id = list(map(lambda x: prefix + '_' + x, self.pe_index_id))
        else:
            self.pe_index_id = prefix + '_' + self.pe_index_id
        super().rename_loop_indices(prefix, indices_to_rename)

def main():
    from SeqTree import SequenceBuilder, ActionNode, LoopNode, DelayNode
    from Atoms.TXAtom import TXAtom
    from Atoms.GradientAtom import GradientAtom
    from Atoms.AtomType import AtomType
    from SequenceDiagram import SequenceDiagram
    from SeqTree.ParameterizedFunction import ParameterizedFunction

    fov_pe = 0.256
    n_pe = 10
    sb = SequenceBuilder.SequenceBuilder()
    med = sb.mediator
    pe_grad = PhaseEncoderNode('PE-grad', med, 500, fov_pe, n_pe, 100, 100, 'pe_idx')


if __name__ == '__main__':
    main()
