"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from anytree import NodeMixin
from typing import List, Optional, Tuple
from .SequenceMediator import SequenceMediator
from abc import ABC, abstractmethod


class SequenceNode(ABC, NodeMixin):

    # color definitions for the DOT exporter
    rf_color = '#0b7de7'  # blue
    adc_color = '#3be8e8'  # turquoise
    compound_color = '#e8648e'  # pink
    delay_color = '#9c00b8'  # violet
    loop_color = '#ffce1c'  # sort of orange
    gradient_base_color = 'blue'  # base color for all gradients
    gs_color = 'yellow'  # slice-select gradient
    gp_color = 'pink'  # phase-encode gradient color
    gr_color = 'green'  # readout gradient color

    def __init__(self, name: str, mediator: SequenceMediator):
        self.name: str = name

        self.children: Tuple['SequenceNode'] = ()
        self.parent: Optional['SequenceNode'] = None
        self.mediator: SequenceMediator = mediator

    # we implement duration as a property rather than a field, since in some cases the duration has to be calculated on the fly
    @property
    def duration(self) -> float:
        raise NotImplementedError()

    def run(self) -> None:
        self.execute()
        self.increment_time()

    @abstractmethod
    def execute(self) -> None:
        raise NotImplementedError()

    def get_duration_of_children(self) -> float:
        """
        Get the cumulative duration of all children, if there are any
        :return: the cumulative duration of all children
        """
        if self.children is None or len(self.children) == 0:
            return 0.0
        return sum([x.duration for x in self.children])

    def increment_time(self) -> None:
        """
        Increment the time of the mediator to represent the end of this node
        :return: Nothing
        """
        # check how much time has been added by the children already:
        children_time = self.get_duration_of_children()
        missing_time = self.duration - children_time
        if missing_time > 0:
            self.mediator.increment_time(missing_time)

    def run_children(self) -> None:
        if self.children is not None:
            for c in self.children:
                c.run()

    def register_value(self, val_id: str, value) -> None:
        full_id = self.name + '/' + val_id
        self.mediator.set_or_update(full_id, value)

    def add_child(self, *args) -> None:
        """
        Add one or more child nodes. New nodes are appended in the order in which they are given to the right of existing
        children.
        :param args: one or multiple node(s), separated by commas (not as a list!)
        :return: nothing
        """

        new_children = tuple(args)
        if self.children is None:
            self.children = new_children
        else:
            old_children = self.children

            self.children = old_children + new_children

    def get_additional_info(self) -> str:
        return 'Additional information...'

    def get_style_and_shape(self) -> str:
        return 'shape=box, style=filled'

    def insert_filltime(self) -> None:
        """
        Insert fill times (for LoopNodes only) to assure that all loops have the desired duration. Only LoopNodes will
        act on this, but we are implementing it here to that the function can be called recursively on all nodes.
        :return: nothing
        """

        for x in self.children:
            x.insert_filltime()

    def get_atom_instances(self, wanted_type, instances=None) -> List['SequenceNode']:
        """
        Return a list of all children where x has an atom, and isinstance(x.atom, wanted_type) is true. Do this
        recursively if the children have children themselves.
        :param wanted_type: an atom type
        :param instances: the instances that have been found so far, for recursive use
        :return: a list of found instances (type: SequenceNode
        """

        if instances is None:
            instances = []

        test_fun = lambda x: hasattr(x, 'atom') and isinstance(x.atom, wanted_type)
        instances += list(filter(test_fun, self.children))

        for c in self.children:
            c.get_atom_instances(wanted_type, instances)  # the children append their instances to the instances list

        return instances

    def delete_child_by_index(self, idx: int) -> None:
        """
        Delete a child node specified by index. If the child does not exist, nothing happens
        :param idx: the index of the child to be deleted
        :return: nothing
        """
        c = list(self.children)  # children could also be a tuple, hence the explicit conversion
        if 0 <= idx < len(c):
            del c[idx]
            self.children = c
        else:
            print(f'{self.name}: Error: no child found at index {idx}.')

    def delete_child_by_atom_type(self, atom_type: type, recursive=False) -> None:
        """
        Remove all child nodes whose atom type is of type atom_type. Since only ActionNodes have atoms, only they can be
        removed.
        :param atom_type: An Atom type, e.g. GradientAtom
        :param recursive: If set to yes, then the operation descends also to the children of the node.
        :return: Nothing
        """

        c = list(self.children)  # children could also be a tuple, hence the explicit conversion
        i = 0
        while i < len(c):  # we have to use a while loop instead of a for loop, since len(c) changes when elements are deleted.
            if hasattr(c[i], 'atom') and isinstance(c[i].atom, atom_type):
                del c[i]
                # we do not increase i in this element, since c[i] will now automatically point to the next element
            else:
                i += 1
        self.children = c

        if recursive:
            for c in self.children:
                c.delete_child_by_atom_type(atom_type, recursive=True)

    def get_loop_indices(self, indices=None) -> List[str]:

        if indices is None:
            indices = []
        for c in self.children:
            indices = c.get_loop_indices(indices)
        return indices

    def rename_loop_indices(self, prefix: str, indices_to_rename=None) -> None:
        """
        Rename the loop indices of all children by appending prefix + "_" to them. This is used when cloning a tree, e.g.,
        to create prepscans.
        :param prefix: a string that will be prepended to each loop index found.
        :param indices_to_rename: a list of all indices found so far. This will be used by all ActionNode instances
        that have to rename their dependencies on dynamic variables.
        :return: nothing
        """

        if indices_to_rename is None:
            indices_to_rename = []
        for c in self.children:
            c.rename_loop_indices(prefix, indices_to_rename)


