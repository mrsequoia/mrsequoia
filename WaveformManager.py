"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from abc import ABC, abstractmethod
from Encoder import Encoder

from DummyEncoder import DummyEncoder


class WaveformManager(ABC):

    def __init__(self):
        self.encoder = DummyEncoder()  # this will be replaced with a proper encoder if the data file can be found.

    # abstract methods that need overriding in implementations
    @staticmethod
    @abstractmethod
    def encoder_from_file(filename: str) -> Encoder:
        raise NotImplementedError('Method WaveformManager.encoder_from_file has to be implemented by derived classes')

    # concrete implementations for querying waveform data
    #def get_interval(self, start: int = None, end: int = None) -> (np.ndarray, np.ndarray):
    #    """
    #    Return the values and the corresponding time axis between two time points. If the time points are omitted,
    #    the entire waveform is returned
    #    :param start: start of the desired interval in µs (defaults to 0, start of the sequence)
    #    :param end: end of the desired interval in µs (defaults to the end ot the sequence)
    #    :return: a tuple (time, values), where each is a one-dimensional array of equal length
    #    """

#        self.encoder_from_file()  # import waveform (if is has not been imported already)

#        return self.encoder.expand(start, end)

 #   def get_value_at_time(self, t: int) -> float:
 #       """
 #       Get the value at time t
 #       :param t: Timepoint in us
 #       :return: value of the waveform at time t
 #       """

 #       self.encoder_from_file()  # import waveform (if is has not been imported already
 #       return self.encoder.get_value_at_time(t)

 #   def get_waveform(self, start_time: int = 0, end_time: int = -1) -> (np.ndarray, np.ndarray):
 #       """
 #       Get the waveform between start time and end time from the encapsulated encoder. If the encoder cannot be
 #       instantiated (e.g., because the output file for that data source does not exist, a dummy encoder is used
 #       instead that produces all zero values).
 #       :param start_time: the start time of the queried time interval in µs
 #       :param end_time: the end time of the queried time
 #       :return: a tuple of 1D arrays: (time, values)
 #       """
 #       self.encoder_from_file()
 #       t, y = self.encoder.get_waveform(start_time, end_time)
 #       return t, y
