import unittest

from Atoms import GradientAtom, AtomType
import numpy as np


class GradientAtomTest(unittest.TestCase):

    def setUp(self) -> None:
        self.amplitude = 10
        self.rut = 100
        self.rdt = 100
        self.duration = 1000
        self.start_time = 300

        self.ga = GradientAtom.GradientAtom(self.start_time, 'TrapGradient', self.amplitude, self.duration, self.rut, self.rdt, AtomType.AtomType.GS)

    def test_attributes(self):

        self.assertTrue(self.ga.amplitude == self.amplitude)
        self.assertTrue(self.ga.start_time == self.start_time)
        self.assertTrue(self.ga.duration == self.duration)
        self.assertTrue(self.ga.axis == AtomType.AtomType.GS)
        self.assertTrue(self.ga.rut == self.rut)
        self.assertTrue(self.ga.plateau_time == self.duration-self.rut)
        self.assertTrue(self.ga.rdt == self.rdt)

    def test_waveform(self):

        t_exp = np.arange(0, self.duration+self.rdt, 10)
        gx_exp = np.zeros(t_exp.shape)
        gy_exp = np.zeros(t_exp.shape)
        gz_exp = np.concatenate((np.arange(0, self.amplitude),
                                 self.amplitude*np.ones((1, int(self.ga.plateau_time/10)-1)),  # the last point of the plateau is part of the down-slope, thus -1 here
                                 np.arange(self.amplitude, 0, -1),
                                 0), axis=None)

        t, g = self.ga.get_waveform_trapezoidal()
        gx = g[0, :]
        gy = g[1, :]
        gz = g[2, :]

        self.assertTrue(np.all(t == t_exp))
        self.assertTrue(np.all(gx_exp == gx))
        self.assertTrue(np.all(gy_exp == gy))

        print('Test')
        self.assertTrue(np.all(gz_exp == gz))

    def test_properties(self):

        self.assertAlmostEqual(self.ga.get_moment(0), 1/2*(self.rut + self.rdt)*self.amplitude + (self.duration-self.rut)*self.amplitude, delta=1e-6)
        self.assertEqual(self.ga.get_center_time(), self.start_time+(self.duration+self.rdt)/2)
        self.assertEqual(self.ga.get_axis_as_string(), 'SS')


