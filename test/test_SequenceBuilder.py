"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
from unittest import mock
from SeqTree.SequenceBuilder import SequenceBuilder
from SeqTree.LoopNode import LoopNode
from SeqTree.DelayNode import DelayNode
from SeqTree.ActionNode import ActionNode

from Atoms.GradientAtom import GradientAtom
from Atoms.TXAtom import TXAtom
from Atoms.ADCAtom import ADCAtom
from Atoms.AtomType import AtomType

from SequenceTiming import SequenceTiming


class SequenceBuilderTest(unittest.TestCase):

    def setUp(self):
        self.sb = SequenceBuilder()
        self.loop1 = LoopNode("Loop1", self.sb.mediator, 'loop_idx1', 1)
        self.loop2 = LoopNode('Loop2', self.sb.mediator, 'loop_idx2', 1)

        self.loop3 = LoopNode('Loop3', self.sb.mediator, 'loop_idx3', 1)

    def build_tree(self):
        """
        Build a tree with the following structure:

                                sb.root
                               /       \
                            loop1    loop3
                              |
                            loop2
        :return: nothing
        """
        self.sb.add_child(self.sb.root, self.loop1)
        self.sb.add_child(self.loop1, self.loop2)
        self.sb.add_child(self.sb.root, self.loop3)

    def test_add_child(self):

        self.sb.add_child(self.sb.root, self.loop1)
        self.sb.add_child(self.loop1, self.loop2)

        self.assertTrue(self.sb.root.children[0] is self.loop1)
        self.assertTrue(self.loop1.children[0] is self.loop2)

        delay = 20

        # we should not be able to add a child with a negative delay:
        self.assertRaises(ValueError, self.sb.add_child, self.sb.root, self.loop3, -20)

        self.sb.add_child(self.sb.root, self.loop3, delay)

        # now self.sb.root should have 3 children: loop1, a delay node, and loop3
        self.assertTrue(len(self.sb.root.children) == 3)
        self.assertTrue(self.sb.root.children[0] is self.loop1)
        self.assertTrue(isinstance(self.sb.root.children[1], DelayNode))
        self.assertEqual(self.sb.root.children[1].duration, delay)
        self.assertTrue(self.sb.root.children[2] is self.loop3)

    def test_build_sequence(self):

        grad = mock.create_autospec(GradientAtom)
        tx = mock.create_autospec(TXAtom)
        adc = mock.create_autospec(ADCAtom)

        rut = 10

        grad.duration = 50
        grad.axis = AtomType.GR
        grad.total_time = rut
        tx.duration = 30
        adc.duration = 30

        n_reps = 10

        loop = LoopNode('TR-loop', self.sb.mediator, 'tr_idx', n_reps)

        self.sb.add_child(self.sb.root, loop)

        grad_node = ActionNode('grad_node', self.sb.mediator, grad)
        tx_node = ActionNode('tx_node', self.sb.mediator, tx)
        adc_node = ActionNode('adc_node', self.sb.mediator, adc)

        self.sb.add_child(loop, tx_node)
        self.sb.add_child(loop, grad_node)
        self.sb.add_child(grad_node, adc_node, 10)

        with mock.patch('SequenceTiming.SequenceTiming.waveforms_from_list') as mocked_waveforms_from_list:
            st, simu_params = self.sb.build_sequence()

            self.assertEqual(mocked_waveforms_from_list.call_count, 1)

            self.assertEqual(len(st.tx), n_reps)
            self.assertEqual(len(st.gro), n_reps)
            self.assertEqual(len(st.adc), n_reps)







if __name__ == '__main__':
    unittest.main()