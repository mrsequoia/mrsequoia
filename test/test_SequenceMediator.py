"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
from numpy.random import randint
from SeqTree.SequenceMediator import SequenceMediator


class SequenceMediatorTest(unittest.TestCase):

    def setUp(self) -> None:
        self.med = SequenceMediator()

    def test_set_and_get(self):

        initial_value = 3
        self.med.set_or_update(self, 'Property1', initial_value)
        self.assertEqual(self.med.get_value('Property1'), initial_value)

        self.med.set_or_update(self, 'Property1', initial_value+1)
        self.assertEqual(self.med.get_value('Property1'), initial_value+1)

    def test_ownership(self):
        """
        This test validates that only the owner of a variable (i.e. the object that registered the variable) is allowed
        to change its value
        :return: nothing
        """

        prop_name = 'MyProperty'
        prop_val = f'I belong to {id(self)}'

        self.med.set_or_update(self, prop_name, prop_val)

        dummy_object = 'I am not allowed to overwrite'  # This string object serves as an object that wants to change
        # the property
        self.assertRaises(KeyError, self.med.set_or_update, dummy_object, prop_name, 'I shall not prevail')

    def test_shared_variable(self):
        """
        Shared variables do not belong to any one object (technically, they belong to the mediator, but ownership is
        not enforced.
        :return: nothing
        """

        prop_name = 'shared_variable'
        prop_value = 'I am shared'
        self.med.set_or_update_shared_variable(prop_name, prop_value)
        self.assertEqual(self.med.get_shared_variable(prop_name), prop_value)

        new_val = 'Now I am different'
        self.med.set_or_update_shared_variable(prop_name, new_val)
        self.assertEqual(self.med.get_shared_variable(prop_name), new_val)

    def test_time(self):

        increments = randint(1, 80, 10)
        s = 0

        for inc in increments:
            self.med.increment_time(inc)
            s += inc
            self.assertEqual(self.med.get_time(), s)

        self.med.clear_timing()
        self.assertEqual(self.med.get_time(), 0)




if __name__ == '__main__':
    unittest.main()
