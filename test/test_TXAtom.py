"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
import numpy as np
from Atoms.TXAtom import TXAtom
import math


class TXAtomTest(unittest.TestCase):
    tol = 1.0e-6
    volts_to_tesla = 1/(2000 * 42.58 * 100)

    def test_rect_pulse(self):
        duration_us = 5120
        flip_angles = np.arange(0, 360, 5)

        for a in flip_angles:
            tx = TXAtom.rect_pulse(0, a, duration_us)
            # conversion from T to V

            self.assertTrue(tx.duration == duration_us)
            # get the waveform from the Atom:
            t, y = tx.waveform0.expand_fully()
            # integrate waveform over time and get resulting flip angle:
            beta = (np.trapz(y, t) * self.volts_to_tesla * 1e-6 * 42.58e6 * 2 * math.pi * 1e-6 / math.pi) * 180

            self.assertTrue(abs(beta-a) < self.tol)


if __name__ == '__main__':
    unittest.main()
