"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
from Atoms.ADCAtom import ADCAtom
from Atoms.GradientAtom import GradientAtom
from Atoms.AtomType import AtomType
from AtomList import AtomList


class AtomListTest(unittest.TestCase):

    def test_getitem(self):

        adc1 = ADCAtom(0, 'ADC0', 1000, 128, None)
        adc2 = ADCAtom(1000, 'ADC1', 1000, 128, None)

        grad1 = GradientAtom(100, 'Grad1', 10, 5000, 100, 100, AtomType.GR)
        grad2 = GradientAtom(1000, 'Grad2', 5, 2000, 200, 200, AtomType.GP)

        default_list = [adc1, adc2, grad1, grad2]

        atom_list = AtomList(default_list)

        # lookup by index -> Returns a single Atom object
        self.assertTrue(atom_list[1] is adc2)  # check for referential identity

        # lookup by slice:
        li = atom_list[0:2]
        for x in range(0, len(li)):
            self.assertTrue(li[x] is default_list[x])

        # lookup by name -> returns an AtomList
        returned = atom_list['Grad1']
        self.assertTrue(len(returned) == 1)
        self.assertTrue(returned[0] is grad1)

        # lookup by selector function -> returns AtomList
        selector = lambda q: q.start_time == 1000
        returned = atom_list[selector]
        self.assertTrue(len(returned) == 2)
        self.assertTrue(returned[0] is adc2)
        self.assertTrue(returned[1] is grad2)


        # verify that other lookups will result in an error
        self.assertRaises(TypeError, atom_list.__getitem__, 3.5)




if __name__ == '__main__':
    unittest.main()



