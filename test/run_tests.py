"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
import os

dirs = [('generic', '.'), ('Siemens-specific', '../SiemensTools/test'), ('Bruker-specific', '../BrukerTools')]


def main():
    for d in dirs:
        if os.path.isdir(d[1]):
            print('Running %s tests:' % d[0])
            run_tests(d[1])


def run_tests(dir: str):
    loader = unittest.TestLoader()
    tests = loader.discover(dir)
    test_runner = unittest.runner.TextTestRunner(verbosity=2)
    test_runner.run(tests)


if __name__ == '__main__':
    main()