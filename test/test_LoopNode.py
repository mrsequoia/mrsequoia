"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
from unittest import mock
from SeqTree.SequenceMediator import SequenceMediator
from SeqTree.LoopNode import LoopNode


class LoopNodeTest(unittest.TestCase):

    def setUp(self):
        self.med = mock.create_autospec(SequenceMediator)

        self.root = LoopNode('root', self.med, idx_id='root_idx', n_runs=1)

        self.runs_loop1 = 5
        self.runs_loop2 = 4
        self.duration_loop1 = 50
        self.duration_loop2 = 30

        self.loop1 = LoopNode('loop1', self.med, idx_id='loop1_idx', n_runs=self.runs_loop1, duration=self.duration_loop1)
        self.loop2 = LoopNode('loop2', self.med, idx_id='loop2_idx', n_runs=self.runs_loop2, duration=self.duration_loop2)

        # The insert_filltime calls would usually be performed by SequenceBuilder, we have to do it manually here:
        self.loop1.insert_filltime()
        self.loop2.insert_filltime()

        self.root.add_child(self.loop1, self.loop2)

    def test_duration(self):

        duration = self.root.duration

        self.assertEqual(duration, self.runs_loop1*self.duration_loop1 + self.runs_loop2*self.duration_loop2)

    def test_run(self):

        # Since a loop node executes run_children once for each run, we mock this method and count the number of calls
        self.loop1.run_children = mock.MagicMock()
        self.loop2.run_children = mock.MagicMock()
        self.root.run()

        self.assertEqual(self.loop1.run_children.call_count, self.runs_loop1)
        self.assertEqual(self.loop2.run_children.call_count, self.runs_loop2)
        self.assertEqual(self.loop1._run_idx, self.runs_loop1-1)
        self.assertEqual(self.loop2._run_idx, self.runs_loop2-1)

    def test_rename_loop_indices(self):

        prefix = 'new'
        old_idx_id_1 = self.loop1.loop_idx_str
        old_idx_id_2 = self.loop2.loop_idx_str

        self.root.rename_loop_indices(prefix)

        self.assertEqual(self.loop1.loop_idx_str, prefix + '_' + old_idx_id_1)
        self.assertEqual(self.loop2.loop_idx_str, prefix + '_' + old_idx_id_2)


if __name__ == '__main__':
    unittest.main()
