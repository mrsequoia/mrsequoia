# Testing the sequence timing
This document demonstrates how to run pre-defined tests on a `SequenceTiming` object. Custom tests can easily be developed by deriving from one of the existing Check classes.

*Note:* In the context of MRSequoia, we refer to timing tests as "checks", to distinguish them from the tests for assessing proper functioning of MRSequoia itself.


In the following code snippets, it is assumed that `st` is a `SequenceTiming` instance that was either constructed from a sequence tree (see [SequenceBuilder.md](SequenceBuilder.md)) or imported from Siemens IDEA simulator output (see [SequenceImport.md](SequenceImport.md))

Check that TR is constant by measuring the time between excitation pulses:
```python
from TRCheck import TRCheck
tr_check = TRCheck(st.tx[0].name)
tr_check.run_check(st)
```
Result:

    Found one value for TR: 100000 µs
    Result: Success!
TRCheck is initialized with either a string identifying one Atom type (``st.tx[0].name`` evaluates to 'SRFExcit'), or a function that selects specific atoms from the list of all atoms. For example, to measure the timing between all RF pulses with a flip angle of 15°:
```python
from TRCheck import TRCheck
from Atoms import TXAtom
selector = lambda x: isinstance(x, TXAtom) and x.angle==15
tr_check = TRCheck(selector)
tr_check.run_check(st)
```
yields the same result:

    Found one value for TR: 100000 µs
    Result: Success!
    
Here we used a `selector` to anchor the TE measurement, i.e., an anonymous boolean function that indicates whether a given `Atom` instance should be selected or not. Since selector is performed on each single `Atom` of the sequence, it is necessary to perform the `isinstance` check, since only TXAtoms possess an `angle` attribute. Omitting this condition would lead to error messages when `selector` is performed against a `GradientAtom` or `ADCAtom`.

You can also check that TR is not only constant, but also has a specific value:
```python
tr_check.run_check(st, 90000)
```
    Found one value for TR: 100000 µs
    Expected TR: 90000
    Result: Epic fail!

If the delays between Atoms may be variable, but only a few discrete values are allowed:
```python
from VariableDelayCheck import VariableDelayCheck
vd_check=VariableDelayCheck('SRFExcit', 'PERew')
allowed_values = [90000, 100000, 32560]
vd_check.run_check(st, allowed_values)
```
checks that the delay between each excitation pulse and the next PE rewinder gradient is 90000, 100000 or 32560 µs:

    VariableDelayCheck: Success!

New checks can be developed in only a few lines of code by deriving from the ``CheckBase`` class.


# Miscellaneous:
## Retrieve the Measurement Data Header (MDH) for an ADC object:
This only works for sequences imported from Siemens IDEA simulator output. For all other sequences, `adc_info` is not populated.

```python
st.adc[0].adc_info.print()
```

    aulEvalInfoMask0          : 8454152                       
    aulEvalInfoMask1          : 0                             
    aushIceProgramPara0       : 0                             
    aushIceProgramPara1       : 0                             
    ...                             
    fReadOutOffcentre         : 0.0                           
    lMeasUID                  : 4294967295                    
    sCutOff.ushPost           : 0                             
    sCutOff.ushPre            : 0                             
    sLC.ushAcquisition        : 0                             
    sLC.ushEcho               : 0                             
    sLC.ushIda                : 0                             
    sLC.ushIdb                : 0                             
    sLC.ushIdc                : 0                             
    sLC.ushIdd                : 0                             
    sLC.ushIde                : 0                             
    sLC.ushLine               : 17                            
    sLC.ushPartition          : 0                             
    sLC.ushPhase              : 0                             
    sLC.ushRepetition         : 0                             
    sLC.ushSeg                : 0                             
    sLC.ushSet                : 0                             
    sLC.ushSlice              : 0                             
    sSD.aflQuaternion 0       : 0.696364                      
    sSD.aflQuaternion 1       : 0.122788                      
    sSD.aflQuaternion 2       : -0.122788                     
    sSD.aflQuaternion 3       : 0.696364                      
    sSD.sSlicePosVec.flCor    : 0.0                           
    sSD.sSlicePosVec.flSag    : 0.0                           
    sSD.sSlicePosVec.flTra    : 0.0                           
    sulFlagsAndDMALength      : 0                             
    ulPMUTimeStamp            : 0                             
    ulScanCounter             : 18                            
    ulTimeSinceLastRF         : 0                             
    ulTimeStamp               : 0                             
    ushKSpaceCentreColumn     : 256                           
    ushKSpaceCentreLineNo     : 128                           
    ushKSpaceCentrePartitionNo: 0                             
    ushPTABPosNeg             : 0                             
    ushSamplesInScan          : 512                           
    ushUsedChannels           : 2      