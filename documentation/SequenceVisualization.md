## Sequence visualization:
*(Note: visualization is only available when using JupyterLab. See Installation.md for more information).*

MRSequoia can visualize the RF and gradient waveforms with support for panning and zooming. Information on all Atoms in the center of the plot (blue vertical line) are displayed in the table below the plot.
```
%matplotlib widget
from SequenceTiming import SequenceTiming
from SequenceDiagram import SequenceDiagram

st=SequenceTiming('SiemensTools/examples/FLASH/SimulationProtocol_INF.dsv')
s=SequenceDiagram(st, cont_update=True)
s.plot()
```
![](Sequence_visualization.png)

## Visualize gradient waveforms:
*(Note: visualization is only available when using JupyterLab. See Installation.md for more information).*

Sinusoidal gradients can be displayed in either logical (PE, RO, SS) or physical (x, y, z, also called scanner coordinates) coordinates. While the sequence specifies gradients in logical coordinates, the simulation outputs gradient waveforms in physical coordinates, and the translation between the two coordinate systems is achieved through multiplication with the gradient rotation matrix (or its inverse).
To show the waveform of the first gradient on the readout axis in physical coordinates:
```python
st.gro[0].plot_trapezoidal()
```
![](ro_grad_waveform_physical.png "")
\

When plot_trapezoidal is run for the first time after instantiating a SequenceTiming object, it is necessary to find the gradient rotation matrix that translates between logical (SS, PE, RO) and physical coordinates (x, y, z). This happens in automatically in the background, however, depending on the duration of the simulated sequence, it might take a while.

To plot a gradient waveform in logical rather than physical coordinates:
```python
st.gro[0].plot_trapezoidal(scanner_coordinates=False)
```
![](ro_grad_waveform_logical.png "")
\ 

## Visualize TX pulse:
Plot magnitude and phase (2 channels) of the first RF pulse:
```python
st.tx[0].plot_waveform()
```
![](tx_waveform_plot.png "")
\ 

## Plot acoustic spectrum:
Show the acoustic vibration spectrum induced by the gradients on the three gradient axes up to 2 kHz:
```python
from Spectrogram import Spectrogram
s = Spectrogram(st)
s.plot_spectra(fmax=2000)
```
![](screenshot_acoustic_spectrum.png "")