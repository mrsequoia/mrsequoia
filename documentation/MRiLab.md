# MRiLab support
[MRiLab](http://mrilab.sourceforge.net) ([Liu et al. 2016](https://ieeexplore.ieee.org/document/7676360)) is a Matlab-based numerical MRI simulator that allows one to define phantoms, sequences and image reconstruction methods and to perform realistic MR simulations on them. MRSequoia can export its internal representation of MR sequences to an XML file that can be imported into MRiLab, allowing for MR physics simulations of prototype sequences assembled using SequenceBuilder, or custom Siemens sequences using the Siemens IDEA simulator output.

**Note:** MRiLab support is so far highly experimental, and there are open issues, especially for IDEA simulator output.

## Usage 
The workflow for simulating a sequence from IDEA in MRiLab is as follows:
1. Simulate the sequence in POET (or ```sim```).
2. Import the simulation results in MRSequoia.
3. Use MRSequoia's MRiLabExporter class to export the sequence to an XML file.
4. Set up MRiLab, import the sequence and run the simulation.

For sequences composed using SequenceBuilder, the steps are as follows:
1. Assemble sequence using SequenceBuilder.
2. Export the sequence to an XML file using MRiLabExporter
3. Load sequence in MRiLab, check automatically imported scanning parameters, start simulation.
For steps 1 and 2, see [SequenceBuilder.md](SequenceBuilder.md)

### In POET:
1. Set up the sequence parameters in the POET window.
2. Make sure that a single-channel TX coil is selected (e.g., by manually selecting the "TxRx CP Head" from the coil menu and activating it in the System-Coils tab). Be aware that some scanner configurations come with a 2ch body coil. In the case of parallel transmit, the RF amplitude is split between the available channels according to some unknown algorithm, so the exported RF amplitudes are physically meaningless and will not lead to the correct flip angles in MRiLab.
3. Either copy the RF reference voltage ("Ref. amplitude 1H") from the System-Tx/Rx tab (if it is set to a non-zero value), or set it to some non-zero voltage manually. This value is needed as the ```ref_voltage``` parameter to be supplied to the MRiLabExporter constructor for correct B<sub>1</sub> calculation in MRiLab.
4. When running the sequence simulation from POET, make sure that "RFP, Signal Phase" simulation option is activated. This is turned off by default and has to be activated manually for each simulation (is there a way to save this as a new default?). **Note**: If signal phase simulation is deactivated, you will not receive an error message from MRiLabExporter, but the phase will be set to zero. This means that the complex RF signal will always be real and positive, which is probably not what you want.

### In MRSequoia:
1. Import the simulation output: ```st = SequenceTiming(sim_file)```
2. Extract the relevant simulation/protocol parameters from the simulation output: ```simu_params = st.get_simu_params()``` 
4. Create an MRiLabExporter object, using the reference voltage as explained in step 3 above: ```exp = MRiLabExporter(st, ref_voltage=100)```
4. Write the sequence XML file: ```exp.write_mrilab_xml(out_dir, 'PSD_FLASH_test.xml', simu_params)```

**Note:** The name of the xml file has to begin with 'PSD_', since otherwise it will not be shown in the sequence selection dialog of MRiLab. 

An example for how to use the MRiLab export can be found in the demo() function in MRiLabExporter.py or in MRiLab_example.ipynb.

### In MRiLab:
If you want to use the default reconstruction function, BasicReco.m, make sure that the containing path (MRiLab-Code) is included in the Matlab path.
![](MRiLab workflow.png)
- Load a phantom for the simulation from the File menu (1)
- Click on the ```< Localizer``` button to load the phantom preview into the slice-positioning window (2)
- Click on ```Update``` (3)
- Load the sequence by clicking on the ```Sequence``` button, then choosing the ```O``` button at the bottom of the dialog and selecting the sequence XML file.
![](MRiLab sequence selection.png)
- Click on ```Update``` again (3)
- Set the sequence parameters. Refer to "MRiLab simulation settings" below for some hints. (5) If the sequence was built using SequenceBuilder, the relevant parameters should already be correct.
- On the ```Hardware``` tab you can set some scanner-specific hardware settings. This might be necessary if you get errors caused by exceeding the default scanner specifications. To avoid gradient-related errors, you can set ```MinUpdRate``` to ```0``` and ```MaxSlewRate``` to ```inf```.
- Click ```Update``` again. (3)
- Click ```Scan``` to start the simulation. After the simulation, the results will be written to the Output directory of the MRiLab installation. The k-space data is contained in ```VSig.Sx``` (real part of complex k-space signal) and ```VSig.Sy``` (imaginary part).

**Note:** Resolution-related parameters (e.g., FOV and bandwidth) have to be entered correctly in the MRiLab interface. TR has to be longer than the total time of the scan, since otherwise the simulation will stop after ```ResPhase*TR```.

## Phase-encode lines and TR - EPI to the rescue
Without additional modules loaded (e.g., the EPI or TSE modules), MRiLab's acquisition model assumes that a single phase-encode step is performed (and thus that a single k-space line is acquired) in each excitation-readout cycle (having a duration of TR). This means that for each phase-encode step, the entire simulated IDEA sequence is executed. For example, if ```ResPhase``` (# of phase-encode lines) on the ```Imaging``` tab is set to 128, the simulator will perform 128 repetitions of the entire sequence, resulting in 128 volume acquisitions. In other words, from MRiLab's perspective, every imported sequence looks like a single-shot sequence (regardless of how segmented the sequence actually is), and the TR value set in MRiLab is the total acquisition time, independent of the actual TR of the sequence.

One solution to this would be to set the ```ResPhase``` parameter to 1, thus only performing a single run of the sequence. However, MRiLab requires ```ResPhase``` to be even, making 2 the lowest possible number. This can be circumvented by commenting out line 103 (```VCtl.ResPhase = VCtl.ResPhase - mod(VCtl.ResPhase,2); % guarantee even number of Ky sample points for Ky = 0```) in ```MRiLab-master/Src/Main/DoPreScan.m``` (but it may lead to unexpected consequences). This works for the acquisition side; however, MRiLab then expects the signal for a slice/segment to have the size [RO samples x 1] and gets [RO samples x PE lines], which can cause issues later on for image reconstruction or when displaying the reconstructed image in MRiLab's main GUI.

A better solution, which is used by default for sequences assembled using SequenceBuilder, is to load the EPI module and set both ```ResPhase``` and the echo train length (ETL) to the actual number of phase-encode lines, thus pretending that the sequence is a single-shot EPI sequence. It does not matter if that's really the case, since the sequence timing itself is taking care of the acquisition scheme, and MRiLab does not need to know about it.

## Gradients and coordinate systems
MRiLab defines the gradient and image coordinate system in terms of three axes: GxR, GyPE, and GzSS. The nomenclature here is a bit misleading, since the three axes refer to the *logical* axes of the coordinate system, not the physical ones. In other words, GxR is always the readout axis, and thus its direction changes depending on slice orientation and prescription of the frequency direction. Furthermore, the MRiLab GUI does not provide a means for scanning angled or rotated slices; only slices parallel/perpendicular to the main axes are possible. To overcome this limitation, arbitrary slice orientations can be prescribed in MRSequoia, the MRiLab-Exporter then distributes the necessary gradients on the three axes defined by MRiLab. In order to achieve a consistent mapping between the gradient axes in MRSequoia and MRiLab, the following convention will be used:

The coordinates are defined based on patient lying prone position, head-first.
- The x-axis connects the ears (left-right).
- The y-axis is along the anterior-posterior axis
- The z-axis is head-feet (parallel to B0).
 
 To achieve this, the simulation settings in MRiLab should always be as follows:
 - ```ScanPlane```: Axial
 - ```FreqDir```: L/R
MRiLabExporter will then take care of translating from the logical axes used in MRSequoia to the physical axes defined above.

## Hints & Troubleshooting 
- MRiLab adds all subfolders of its Output directory to the Matlab path, as well as the path to the sequence XML file. This might lead to unexpected results, i.e., "load Series3.mat" in Matlab could load a file from a directory other than the current one. It is therefore recommended to delete these additional paths from the Matlab path before starting MRiLab. Furthermore, loading a file using an absolute or relative path rather than just the file name can help to avoid this problem.
- Related to the previous point, it may also happen that the wrong gradient waveforms (gradient_x/y_z.mat) are loaded if such files exists in more than one directory contained in the Matlab path list. To avoid this, all gradient/RF pulse waveform files have the timestamp of the creation time appended to their filename, to make the names unique across all folders.
- The very first and very last ADCs (k-space points) are set to zero in each simulation. This means that the acquired k-space has two less points than expected. This is mentioned somewhere in the documentation, but not in a very prominent position.
- The ExtBit mechanism does not seem to work, i.e., resetting k-space positions, introducing perfect spoiling etc. seem to be broken. As a consequence, k-space trajectories (as defined by ```VSig.Kx/Ky/Kz```) are offset.
- If the standard image reconstruction of MRiLab cannot deal with your k-space data, you can either select your own reconstruction function or set the ```AutoRecon``` parameter to ```off``` (both settings can be found on the ```Recon``` tab). MRSequoia comes with a very basic reconstruction script (```BasicReco.m```), which can be selected by setting ```ReconEngine``` to 'external' and entering 'BasicReco' (including the single quotes!) in the ```ReconEng``` field. This requires *MRSequoia/MRiLab-Code* to be in your Matlab path. 
- In single-slice imaging, the thickness of the phantom has to be larger than the slice thickness, otherwise an empty image is acquired.

#### MRiLab simulation settings
- *ResPhase:* this parameter encodes the number of phase-encode steps in MRiLab. See section *Phase-encode lines and TR - EPI to the rescue* above for an explanation.
- *TR:* has to be longer than the total time of the scan, since otherwise the simulation will stop after ```ResPhase * TR``` (or after TR, if the EPI module is used).

## References
* F. Liu, J.V. Velikina, W.F. Block, R. Kijowski, A.A. Samsonov. Fast Realistic MRI Simulations Based on Generalized Multi-Pool Exchange Tissue Model. IEEE Transactions on Medical Imaging. 2016. doi: 10.1109/TMI.2016.2620961. https://ieeexplore.ieee.org/document/7676360