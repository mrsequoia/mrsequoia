#!/bin/bash

kernel_name=MRSequoia

pipenv run python -m ipykernel install --user --name=$kernel_name

echo "The new Jupyter kernel has been installed as '$kernel_name'. You can now start JupyterLab, open one of the notebooks and select the MRSequoia kernel."
