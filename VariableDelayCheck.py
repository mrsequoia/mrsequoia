"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from typing import List

from CheckBase import CheckBase
from SequenceTiming import SequenceTiming
from Types import SelectorType


class VariableDelayCheck(CheckBase):
    """
    VariableDelayTest: A test to check that the delay between two atoms only takes values from a predefined
    set of allowed values. This could be used, for example, in a sequence with multiple TI or TE values to check that
    no unexpected TI/TE values are used. This is similar to ConstantDelayTest, but more than one "correct" values
    are allowed.
    """

    def __init__(self, designator1: SelectorType, designator2: SelectorType, skip1: int = 0, skip2: int = 0, name: str = None):
        """
        The constructor. See documentation of ConstantDelayTest for details
        :param designator1: a selector (string or function Atom -> bool) for selecting the first type of atom
        :param designator2: ditto for the second atom
        :param skip1: skip the first n atoms matching designator1
        :param skip2: skip the first n atoms matching designator2
        :param name: the name of the test to be used when reporting results
        """
        if name is None:
            name = 'VariableDelayCheck'
        super().__init__(name)
        self.designator1: SelectorType = designator1
        self.designator2: SelectorType = designator2
        self.skip1: int = skip1
        self.skip2: int = skip2

    def run_check(self, timing: SequenceTiming, allowed_values: List[int]) -> bool:
        """
        Run the test.
        :param timing: the SequenceTiming object
        :param allowed_values: a list of "allowed" values for the timinig between the two event types
        :return: a boolean indicating whether the test was successful
        """

        ev1 = self.filter_by_designator(self.designator1, timing)
        ev2 = self.filter_by_designator(self.designator2, timing)

        if self.skip1 > 0:
            ev1 = self.skip_first_atoms(ev1, self.skip1)
        if self.skip2 > 0:
            ev2 = self.skip_first_atoms(ev2, self.skip2)

        if len(ev1) != len(ev2):
            print('Error: The two designators yielded different numbers of events.')
            print('designator1: ' + str(len(ev1)) + ' atoms')
            print('designator2: ' + str(len(ev2)) + ' atoms')
            return False

        delays = [ev2[i].start_time - ev1[i].start_time for i in range(0, len(ev1))]

        # convert the list of all delay values to a set to retain only unique values and discard duplicates
        s = set(delays)

        success = True
        # check that all values in the result set are contained in the list of allowed values
        for val in s:
            if val not in allowed_values:
                print('Value ' + str(val) + ' is not in the set of allowed values.')
                success = False

        self.print_success_or_fail(success)
        return success


if __name__ == '__main__':
    # a usage example
    from Atoms.SyncAtom import SyncAtom
    file = 'SimOutput/FLASH/SimulationProtocol_INF.dsv'
    st = SequenceTiming(file)
    allowed_values = [2000, 3000, 4000]
    designator1 = 'SRFExcit'
    designator2 = lambda x: isinstance(x, SyncAtom) and x.name == 'Osc0'  # does the same as designator2 = 'Osc0'

    test = VariableDelayCheck(designator1, designator2, 0, 0, 'My variable delay test')
    test.run_check(st, allowed_values)
