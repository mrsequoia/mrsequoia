import numpy as np
from typing import Tuple


class DummyEncoder:

    def __init__(self):
        pass

    def get_interval(self, start_time: int = 0, end_time: int = None):

        if end_time is None:
            t = np.asarray([start_time])
            y = np.asarray([0])
        else:
            t = np.asarray([start_time, end_time])
            y = np.asarray([0, 0])
        return t, y

    def expand(self, start_time: int = 0, end_time: int = None):
        # how do we expand into a time series without knowing the sampling rate?
        return self.get_interval(start_time, end_time)

    def expand2(self, start_time: int = 0, end_time: int = -1) -> (np.ndarray, np.ndarray):
        pass

    def get_value_at_time(self, t: int) -> float:
        return 0

    def get_waveform(self, start_time: int = 0, end_time: int = -1) -> Tuple[np.ndarray, np.ndarray]:
        return np.asarray([]), np.asarray([])

    def finalize(self):
        pass
