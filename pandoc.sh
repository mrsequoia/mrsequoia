#!/bin/bash

for f in *.md; do
	bn=$(basename $f .md)
	echo "Processing $f"
	pandoc $f -t latex -o $bn.pdf
done
#pandoc Readme.md -f markdown -o Readme.html
