"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from SequenceTiming import SequenceTiming
import os
from Atoms.AtomType import AtomType
from Atoms import TXAtom
from AtomList import AtomList
import numpy as np
import scipy.io  # for writing Matlab files
import math
from datetime import datetime as dt
from ExtBit import ExtBit
from math import ceil
from sortedcontainers import SortedList


class MRiLabExporter:

    def __init__(self, st: SequenceTiming, ref_voltage: float = 100):
        self.st: SequenceTiming = st
        self.ref_voltage: float = ref_voltage
        self.creation_date = dt.now()
        self.creation_time_id = self.creation_date.strftime('%Y%m%d-%H%M%S')

    def write_mrilab_xml(self, path: str, filename: str, simu_params: dict = None) -> None:
        """
        Write an XML file of the sequence that can be read by MRiLab
        :param path: the path where to save the file
        :param filename: the name of the file
        :param simu_params: a dict with the parameters for the SimuAttr.xml file. If not specified, the file will not
        be written.
        :return: nothing
        """

        date_str = self.creation_date.strftime('%Y-%m-%d %H:%M:%S')
        adc_phases_file = os.path.join(path, 'ADC_phases.mat')

        template_start = f"""<?xml version="1.0" encoding="utf-8"?>
<MRiLabSeq Name="MRSequoia_Sequence" Notes="Automatically converted sequence, conversion date: {date_str}">
   <CVs CV1="'{adc_phases_file}'" CV10="0" CV11="0" CV12="0" CV13="0" CV14="0" CV2="20e-3" CV3="0"
        CV4="0"
        CV5="0"
        CV6="0"
        CV7="0"
        CV8="0"
        CV9="0"/>
   <Specials EPI="^1"/>
   <Pulses Freq="1" Notes="regular TR section" Switch="$1'on','off'" TREnd="Inf"
           TRStart="1"
           tE="VCtl.TR"
           tS="0">
"""
        template_end = f"""
    </Pulses>
</MRiLabSeq>
"""
        if not os.path.exists(path):
            os.makedirs(path)

        with open(os.path.join(path, filename), 'w') as outfile:
            outfile.write(template_start)

            # Matlab has this annoying "feature" that if .mat files with the same name exists in multiple directories,
            # there is not guarantee for which one will be loaded when a load command without full path is issued.
            # To avoid this, we encode creation date and time into the filename of all gradient/rf pulse files
            # to prevent loading those files from the wrong directory.

            outfile.write("<rf>\n")
            for r in self.st.tx:
                pulse_filename = self.write_pulse_file_matlab(r, path, postfix=self.creation_time_id)
                outfile.write(r.to_mrilab_xml(pulse_filename))
            outfile.write("</rf>\n")

            outfile.write("<GzSS>\n")
            #gz_user = self.user_gradient_xml(AtomType.GZ, path)
            gz_user = self.gradient_xml(AtomType.GZ, path)
            outfile.write(gz_user)
            outfile.write("</GzSS>\n")

            outfile.write("<GyPE>\n")
            #gy_user = self.user_gradient_xml(AtomType.GY, path)
            gy_user = self.gradient_xml(AtomType.GY, path)
            outfile.write(gy_user)
            outfile.write("</GyPE>\n")

            outfile.write("<GxR>\n")
            #gx_user = self.user_gradient_xml(AtomType.GX, path)
            gx_user = self.gradient_xml(AtomType.GX, path)
            outfile.write(gx_user)
            outfile.write("</GxR>\n")

            outfile.write("<ADC>\n")
            for g in self.st.adc:
                outfile.write(g.to_mrilab_xml())
            outfile.write("</ADC>\n")

            outfile.write("\t<Ext>\n")

            ext_bits = []

            # There has to be at least one ExtBit in the xml file, otherwise MRiLab is going to complain. So we insert
            # a dummy ExtBit for calculating the remaining simulation time, if necessary.
            if len(ext_bits) == 0:
                ext_bits = [ExtBit(0, ExtBit.TIMER)]
            ext_bit_xml = "\n".join([x.to_xml() for x in ext_bits])
            outfile.write(ext_bit_xml)
            outfile.write("\n\t</Ext>")

            outfile.write(template_end)

        if simu_params is not None:
            simu_xml = self.generate_simu_xml(simu_params)
            with open(os.path.join(path, 'SimuAttr.xml'), 'w') as simufile:
                simufile.write(simu_xml)

        # for each ADC, write the ADC phase to file for reconstruction in matlab.

        fre_sorted = SortedList(self.st.fre, key=lambda x: x.start_time)

        # define a binary search function to return the index of a FreqPhase atom for a given start time
        def find_index(t: int) -> int:
            ind = fre_sorted.bisect_key_left(t)
            if fre_sorted[ind].start_time != t:
                raise LookupError(f'Could not find atom with start time {t} in list.')
            return ind

        n_adc = len(self.st.adc)
        adc_phases = np.empty((n_adc, 1))

        for i in range(0, n_adc):
            # find the relevant FreqPhase Atom for the ADC:

            if len(self.st.fre) == 0:  # there is not frequency/phase information stored in the timing, so no need to
            # look for it
                pha = 0

            else:
                adc = self.st.adc[i]
                try:
                    idx = find_index(adc.start_time)
                    pha = (self.st.fre[idx].phase)/360 * 2 * math.pi
                except LookupError:
                    print(f'Could not find an FreqPhase atom with start time {adc.start_time}')
                    pha = 0  # if we cannot find a corresponding freq/phase object, we set the phase to 0

            adc_phases[i] = pha

        scipy.io.savemat(adc_phases_file, mdict={'adc_phases': adc_phases})
        self.write_epi_xml(path)

    def write_epi_xml(self, path: str) -> None:
        """
        Write EPI.xml configuration file for MRiLab
        :param path: The output path
        :return: nothing
        """
        # We configure the EPI for single-shot mode, i.e., echo train length = # phase-encode lines.
        xml = """<?xml version="1.0" encoding="utf-8"?>
<EPI EPI_ESP="1e-3" EPI_ETL="VCtl.ResPhase" EPI_EchoShifting="$2'on','off'" EPI_ShotNum="1"/>"""

        with  open(os.path.join(path, 'EPI.xml'), 'w') as outfile:
            outfile.write(xml)

    @staticmethod
    def upsample_gradient_waveform(t: np.ndarray, y: np.ndarray) -> (np.ndarray, np.ndarray):
        """
        Upsample a waveform for exporting it as a UserGradient for MRiLab. We use two different time resolutions:
        dt_plateau for plateaus, and dt_ramp for ramps (really, anything that is not a plateau). This helps to reduce
        file size and simulation time, since plateaus do not need to be encoded at the hightest resolution
        :param t: the time vector, in µs
        :param y: the gradient amplitude
        :return: a tuple (time_interp, y_interp) with the interpolated (upsampled) time and y axis
        """
        plateau_thresh = 1e-6  # the y-threshold between successive points to determine whether there is a plateau
        # between those points
        res_plateau = 100  # in µs
        res_ramp = 0.01

        #  To avoid having a bottleneck from continuously growing the array of upsampled values, we first calculate the
        #  final length the array before initializing it. This is done by summing up the length of all segments.
        def calculate_segment_length(t0, t1, y0, y1) -> int:

            if t0 >= t1:
                return 0
            if abs(y0 - y1) > 1e-6:  # ramp
                return ceil((t1 - t0) / res_ramp)
            return ceil((t1 - t0) / res_plateau)

        count = 0
        for i in range(0, len(t) - 1):  # calculate length of upsampled array
            count += calculate_segment_length(t[i], t[i + 1], y[i], y[i + 1])
        count += 1  # add last point, which will be appended in the end

        tt = np.zeros((count,))
        yy = np.zeros((count,))

        c = 0
        for i in range(0, len(t) - 1):

            if abs(y[i + 1] - y[i]) > plateau_thresh:  # we found a ramp:
                res = res_ramp
            else:
                res = res_plateau
            t0 = t[i]
            t1 = t[i + 1]
            ti = np.arange(t0, t1, res)
            yi = np.interp(ti, [t0, t1], [y[i], y[i + 1]])

            n = len(ti)

            tt[c:c + n] = ti
            yy[c:c + n] = yi
            c += n

        # append the last data point:
        tt[-1] = t[-1]
        yy[-1] = y[-1]

        return tt, yy

    def gradient_xml(self, axis: AtomType, path: str) -> str:
        """
        Export non-overlapping trapezoidal gradients as parameterized gradient shapes. All other gradients have to be
        exported as UserGradients with their waveforms attached.
        :param axis: indicating which axis (GX, GY, GZ) to export
        :param path: the path for the gradient waveform file
        :return: an XML representation
        """

        # TODO: This does not work with rotated slices yet!!!

        xml = ''
        overlapping_gradients = AtomList([])
        if axis == AtomType.GX:
            gradients = self.st.gro
        elif axis == AtomType.GY:
            gradients = self.st.gpe
        elif axis == AtomType.GZ:
            gradients = self.st.gss
        else:
            print(f'Error: Unknown AtomType: {axis}.')
            return ''
        for x in gradients:
            if x.is_trapezoidal() and not self.st.is_overlapping(x, gradients):
                xml += x.to_xml_trapezoidal()

        # do something with overlapping gradients here.
        return xml



    def user_gradient_xml(self, axis: AtomType, path: str) -> str:

        directions = {AtomType.GX: 'Gx', AtomType.GY: 'Gy', AtomType.GZ: 'Gz'}
        grad_dir = directions[axis]

        filename = self.write_gradient_file_matlab(axis, path, postfix=self.creation_time_id)

        xml = f"""<{grad_dir}User DupSpacing="0" Duplicates="1" {grad_dir}File="'{filename}'" 
                 Notes="{grad_dir} gradient waveform" Switch="$1'on','off'"/>
        """
        return xml

    def write_gradient_file_matlab(self, axis: AtomType, path: str, postfix: str) -> str:
        """
        Write the gradient waveform for a given axis
        :param axis: The axis to export
        :param path: The path where to store the output file (excluding the filename)
        :param postfix: A postfix for the output filename (before .mat)
        :return: the filename (excluding the path)
        TBD: The axis logic is a bit problematic now, since for tilted slices, two or three physical gradient axes might be combined into one logical axis. It would make more sence to reference the *output* axis (e.g., GxRO, GyPE, GzSS)
        """

        def interpolate(tt, yy):
            ti = np.arange(tt[0], tt[-1] + 1)  # calculating in µs
            ampl_i = np.interp(ti, tt, yy)
            return ti, ampl_i

        rot_mat = self.st.rot_matrix

        gx = self.st.wf_grx
        gy = self.st.wf_gry
        gz = self.st.wf_grz

        if axis == AtomType.GX:
            # writing GxRO
            comb = rot_mat[0, 0] * gx + rot_mat[0, 1] * gy + rot_mat[0, 2] * gz
            t, ampl = comb.get_waveform()
            t, ampl = self.upsample_gradient_waveform(t, ampl)
            filename = f'''gradient_x_{postfix}.mat'''
        elif axis == AtomType.GY:
            # writing GyPE
            comb = rot_mat[1, 0] * gx + rot_mat[1, 1] * gy + rot_mat[1, 2] * gz
            t, ampl = comb.get_waveform()
            t, ampl = self.upsample_gradient_waveform(t, ampl)
            filename = f'''gradient_y_{postfix}.mat'''
        elif axis == AtomType.GZ:
            # writing GzSS
            comb = rot_mat[2, 0] * gx + rot_mat[2, 1] * gy + rot_mat[2, 2] * gz
            t, ampl = comb.get_waveform()
            t, ampl = self.upsample_gradient_waveform(t, ampl)
            filename = f'''gradient_z_{postfix}.mat'''
        else:
            raise ('Invalid gradient axis: ' + str(axis))

        t = t / 1.0e6  # re-scale from µs to s
        ampl = ampl / 1000  # rescale gradient amplitude from mT/m to t/m

        full_path = os.path.join(path, filename)
        scipy.io.savemat(full_path, mdict={'GTime': t, 'GAmp': ampl})

        return filename

    def write_pulse_file_matlab(self, pulse: TXAtom, path: str, postfix: str) -> str:
        """
        Write a Matlab file representing a TX pulse that can be imported into MRiLab
        :param pulse: the pulse object
        :param path: the path into which to write the file
        :param postfix: a postfix string to be appended to the end of the filename for linking .mat and .xml files.
        :return: the filename (relative to path)
        """

        t, ampl, t_pha, pha = pulse.get_waveform(0)

        # upsample the waveform from 1 µs to 10 ns resolution:
        upsampling_factor = 1
        n_points = int((t[-1] - t[0]) * upsampling_factor) + 1
        t_ups = np.linspace(t[0] * upsampling_factor, t[-1] * upsampling_factor, n_points, endpoint=True)
        t_ups /= upsampling_factor

        ampl_ups = np.interp(t_ups, t, ampl)

        pha_ups = np.interp(t_ups, t_pha, pha)

        t_ups = t_ups / 1.0e6  # re-scale from µs to s
        filename = f'''rfpulse_{pulse.start_time}_{postfix}.mat'''
        full_path = os.path.join(path, filename)

        # conversion factor between voltage (in POET) and B1 amplitude:
        # B1[uT] = ( 90deg / ( 500us * ( 360deg * 42.6MHz/T )  ) * ( B1[V] / ReferenceVoltage[V] )
        # B1[uT] = 11.7uT * ( B1[V] / ReferenceVoltage[V] )
        # (see https://mr-idea.com/communities/idea/Lists/Discussion/Threaded.aspx?RootFolder=%2Fcommunities%2Fidea%2FLists%2FDiscussion%2FConversion between RF pulse voltage and B1 amplitude in simulator&FolderCTID=0x012002005635B5AD41B3E240BA0B9023C8B21F60)

        ampl_ups = 90 / (500e-6 * 360 * 42.58e6) * ampl_ups / self.ref_voltage

        # apply offset phase and frequency:
        pha_ups += pulse.total_phase/360 * 2 * math.pi

        freq = np.full_like(pha_ups, pulse.offset_freq)

        # Avoid baseline effects by setting the first and last points of the waveform to zero:
        # rfAmp(1)=0;
        # rfAmp(end)=0;
        # rfPhase(1)=0;
        # rfPhase(end)=0;
        # rfFreq(1)=0;
        # rfFreq(end)=0;
        # (see http://mrilab.sourceforge.net/manual/MRiLab_User_Guide_v1_3/MRiLab_User_Guide.html section 5.1.3)
        ampl_ups[0] = 0
        ampl_ups[-1] = 0
        pha_ups[0] = 0
        pha_ups[-1] = 0
        freq[0] = 0
        freq[-1] = 0

        scipy.io.savemat(full_path, mdict={'rfTime': t_ups, 'rfAmp': ampl_ups, 'rfPhase': pha_ups, 'rfFreq': freq})
        return filename

    def generate_ext_bits(self, cond2type: dict, position: str) -> list:

        if position not in ['start', 'end', 'center']:
            print('Error in generate_ext_bits: parameter "position" has to be "start", "end" or "center"')
            return []

        conditions = cond2type.keys()

        bits = []

        # for each conditions, find all atoms that match it
        for c in conditions:
            matching = self.st.filter_atoms(c)
            for x in matching:

                start_time = 0
                if position == 'start':
                    start_time = x.start_time
                elif position == 'end':
                    start_time = x.start_time + x.total_time
                elif position == 'center':
                    start_time = x.start_time + x.total_time/2

                bits += [ExtBit(start_time, *cond2type[c])]

        return bits

    def ext_bit_xml(self):

        cond = {'sGSliSel': 6}

        ext_bits = self.generate_ext_bits(cond, 'start')

        xml_list = [f'''\t\t<ExtBit DupSpacing="0" Duplicates="1" Ext="{eb[1]}" Notes="" Switch="$1'on','off'" tStart="{eb[0]/1e6:.6f}"/>''' for eb in ext_bits]
        xml = "\n".join(xml_list)
        return xml

    @staticmethod
    def generate_simu_xml(values: dict) -> str:
        """
        Generate the text for the simulation_attr.xml file for MRiLab using values from the dictionary values
        :param values: a dict with simulation parameters
        :return: a string holding the XML code for the simulation attribute file
        """

        xml = f'''<?xml version="1.0" encoding="utf-8"?>
                    <SimuAttr Description="Simulation Setting Panel Attributes">
                    <Imaging BandWidth="{values['bandwidth']}"
                    FOVFreq="{values['fov_freq']}" 
                    FOVPhase="{values['fov_phase']}"
                    FlipAng="{values['flip_angle']}"
                    FreqDir="{values['freq_dir']}"
                    ResFreq="{values['res_freq']}"
                    ResPhase="{values['res_phase']}"
                    ScanPlane="{values['scan_plane']}"
                    SliceNum="{values['slice_num']}"
                    SliceThick="{values['slice_thick']}"
                    TE="{values['te']}"
                    TEPerTR="1"
                    TR="{values['tr']}"/>

                    <Advanced MasterTxCoil="1" MultiTransmit="$1'off','on'" NEX="1"
                    NoFreqAlias="$2'off','on'"
                    NoPhaseAlias="$2'off','on'"
                    NoSliceAlias="$2'off','on'"
                    Shim="$1'Auto', 'Manual'"
                    TEAnchor="$2'Start', 'Middle', 'End'"/>

                    <Hardware B0="2.89" B1Level="1e-6" E1Level="1e-6" MaxGrad="80e-3" MaxSlewRate="inf"
                    MinUpdRate="0"
                    Model="Siemens PrismaFit"
                    NoiseLevel="0"
                    SpinPerVoxel="1"/>

                    <Recon AutoRecon="$2'off','on'" ExternalEng="'BasicReco'"
                    OutputType="$1'MAT','ISMRMRD','Both'"
                    ReconEng="$2'Default','External'"
                    ReconType="$1'Cartesian','NonCart'"/>
                    </SimuAttr>'''
        return xml


# demonstrate functionality from commandline:
def demo():
    from SequenceTiming import SequenceTiming
    import os

    sim_file = os.path.expanduser('SiemensTools/examples/FLASH/SimulationProtocol_INF.dsv')
    st = SequenceTiming(sim_file)

    exp = MRiLabExporter(st, ref_voltage=100)

    # write the output to the user's tmp dir - make sure that it exists! ;)
    out_dir = os.path.expanduser('~/tmp/FLASH_test')
    exp.write_mrilab_xml(out_dir, 'PSD_FLASH_test.xml')  # The xml file has to start with PSD_, otherwise it will not
    # be shown in the MRiLab sequence selection dialog.


if __name__ == '__main__':
    demo()