from math import pi

gamma_bar = 42.58e6  # = gamma/2pi in Hz/T
gamma = 2*pi*42.58e6  # gyromagnetic ratio in Hz/T
gamma_mT_µs = gamma * 1e-9