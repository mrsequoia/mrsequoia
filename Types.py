"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from typing import Union, Callable

from Atoms.AtomBase import AtomBase

"""
Some type aliases to be used throughout the code
"""

# selector: either a string or a function with signature Atom -> bool
SelectorType = Union[str, Callable[[AtomBase], bool]]
