"""
Written by Sebastian Hirsch.
sebastian.hirsch@bccn-berlin.de
See LICENSE.txt for licensing information.
"""

from SequenceTiming import SequenceTiming
from AtomList import AtomList
from Types import SelectorType


class CheckBase:
    """
    The base class for all tests. Implements some functionality common to all tests, but should not be instantiated
    directly. Instead, derived classes should be used to implement new tests.
    """

    def __init__(self, name: str):
        """
        The extremely boring default constructor
        :param name: The name of the test. Will be used when reporting the test results.
        """
        self.name = name

    def print_success_or_fail(self, status: bool) -> None:
        """
        Print a message indicating the result of the test.
        :param status: a boolean indicating success (True) or failure (pass)
        :return: nothing
        """
        if status:
            print(self.name + ': Success!')
        else:
            print(self.name + ': Epic fail!')

    @staticmethod
    def skip_first_atoms(atoms: AtomList, skip: int) -> AtomList:
        """
        Removes the first few elements from a list and returns a (shallow) copy of the reduced list
        :param atoms: a list of atoms
        :param skip: how many elements to skip at the beginning of the list
        :return: the reduced list
        """
        if skip > len(atoms):
            print('Error: Cannot skip the first %d items in a list of length %d' % (skip, len(atoms)))
            return AtomList([])
        else:
            return atoms[skip:]

    @staticmethod
    def filter_by_designator(designator: SelectorType, timing: SequenceTiming) -> AtomList:
        """
        Filter the list of all atoms contained in timing by a designator. Designator can be either a
        name against which the atom names are matched, or a function reference with signature
        Atom -> bool.
        :param designator: a string or a function reference with signature Atom -> Bool
        :param timing: a SequenceTiming object
        :return: a list of atoms that are compatible with the chosen designator
        """

        try:
            ev = timing.filter_atoms(designator)
            print('Found ' + str(len(ev)) + ' atoms')
        except TypeError:
            print('Unknown designator type: ' + str(type(designator)))
            print('Check EventList.__getitem__ for valid selector types')
            ev = []

        if len(ev) == 0:
            print('Error: No matching atoms found.')
        return ev
