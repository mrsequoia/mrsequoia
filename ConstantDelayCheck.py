"""
Written by Sebastian Hirsch.
sebastian.hirsch@bccn-berlin.de
See LICENSE.txt for licensing information.
"""

from SequenceTiming import SequenceTiming
from CheckBase import CheckBase
from Types import SelectorType


class ConstantDelayCheck(CheckBase):
    """
    ConstantDelayTest: A test  to verify that the delay between two events (e.g., an excitation pulse and a refocusing
    pulse) does not change. Two lists of atoms will be constructed, as described by designator1
    and designator2. It is assumed that both lists have the same length, however, the optional parameters skip1
    and skip2 can be specified to skip the first n objects from each list, to compensate for e.g. pre-scans. Both
    lists of events (after removing the skipped elements, if applicable) must have the same number of elements.
    The test then consists in verifying that
        list2[i].start_time - list1[i].start_time
    is constant for all 0 <= i < len(list1).
    """

    def __init__(self, designator1: SelectorType, designator2: SelectorType, skip1: int = 0, skip2: int = 0,
                 name: str = None):
        """
        Initialize a new test object. The two designators are used to extract atoms from the list of all atoms.
        :param designator1: either a string identifying an atom name (e.g., 'ExcitationPulse'), or a function
        reference with signature Atom -> bool to select events from the list of all atoms.
        :param designator2: ditto, for the second list of atoms
        :param skip1: skip the first n elements in the list specified by designator1
        :param skip2: ditto for the second list
        :param name: an optional name for the test to be used in reports.
        """
        if name is None:
            name = 'ConstantDelayCheck'
        super().__init__(name)
        self.designator1: SelectorType = designator1
        self.designator2: SelectorType = designator2
        self.skip1 = skip1
        self.skip2 = skip2
        self.delays: list = []

    def run_check(self, timing: SequenceTiming) -> bool:
        """
        Run the test.
        :param timing: the SequenceTiming object holding the list of all atoms.
        :return: a boolean value indicating whether the test was passed (True) or not (False).
        """
        ev1 = self.filter_by_designator(self.designator1, timing)
        ev2 = self.filter_by_designator(self.designator2, timing)

        if self.skip1 > 0:
            ev1 = self.skip_first_atoms(ev1, self.skip1)

        if self.skip2 > 0:
            ev2 = self.skip_first_atoms(ev2, self.skip2)

        if len(ev1) != len(ev2):
            print('Error: The two designators yielded different numbers of events.')
            print('designator1: ' + str(len(ev1)) + ' atoms')
            print('designator2: ' + str(len(ev2)) + ' atoms')
            return False

        self.delays = [ev2[i].start_time - ev1[i].start_time for i in range(0, len(ev1))]

        s = set(self.delays)

        if len(s) == 1:
            success = True
            print('Only one value for the delay was found: ' + str(self.delays[0]))
        else:
            success = False
            print('The test found ' + str(len(s)) + ' different values for the delay:')
            distinct_values = list(map(lambda x: str(x), sorted(s)))
            print(", ".join(distinct_values))

        self.print_success_or_fail(success)
        return success

    def print_delays(self) -> None:
        """
        Print the values of the delays in chronological order
        :return: nothing
        """
        print(", ".join([str(delay) for delay in self.delays]))


if __name__ == '__main__':
    # sample usage
    from Atoms.SyncAtom import SyncAtom
    file = 'SimOutput/FLASH/SimulationProtocol_INF.dsv'
    st = SequenceTiming(file)
    allowed_values = [2000, 3000, 4000]
    designator1 = 'SRFExcit'
    designator2 = lambda x: isinstance(x, SyncAtom) and x.name == 'Osc0'  # does the same as designator2 = 'Osc0'

    test = ConstantDelayCheck(designator1, designator2, 0, 0, 'My constant delay test')
    test.run_check(st)
