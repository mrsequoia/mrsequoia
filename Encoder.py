from abc import ABC, abstractmethod
import numpy as np


class Encoder(ABC):  # abstract class, no need to test -> # pragma: no cover

    def __init__(self):
        pass

    @abstractmethod
    def get_value_at_time(self, t: int) -> float:
        raise NotImplementedError('This function has to be implemented by a derived class')

    def get_interval(self, start: int = None, end: int = None) -> (np.ndarray, np.ndarray):
        return self.get_waveform(start, end)

    @abstractmethod
    def get_waveform(self, start: int = None, end: int = None) -> (np.ndarray, np.ndarray):
        raise NotImplementedError()

    @abstractmethod
    def expand_fully(self,  start_time: int = 0, end_time: int = -1) -> (np.ndarray, np.ndarray):
        raise NotImplementedError()


