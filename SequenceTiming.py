"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import math
from math import pi
from typing import List, Tuple, Optional

import numpy as np

from AtomList import AtomList
from Atoms.AtomType import AtomType
from Atoms.GradientAtom import GradientAtom
from Atoms.AtomBase import AtomBase
from Importer import Importer

from SimulationProtocol import SimulationProtocol
from Encoder import Encoder

# TODO. Remove import!
from LineEncoder import LineEncoder


class SequenceTiming:
    """
    SequenceTiming: The class that holds it all together. SequenceTiming contains a list of all atoms of a
    simulated sequence. The atoms are further sorted into sublists, one list for each Atom type (TXAtom,
    GradientAtom, etc.). All lists are sorted by the atom start time. Furthermore, SequenceTiming holds the waveform
    data for the TX and physical gradient channels.
    """

    print('Disclaimer: While a lot of effort has gone into development of this software, it still has to be considered '
          'unvalidated. By using this software you agree to not hold its developer(s) responsible for any harm or '
          'damage that might result from deploying a custom sequence. Ensuring the safety of a sequence is the sole '
          'responsibility of the sequence developer(s).')

    def __init__(self, filename: str = None):
        """
        The constructor for a SequenceTiminig object.
        :param filename: The name (including the path, absolute or relative) of the _INF.dsv file.
        """
        super().__init__()

        self.wf_grx: Optional[Encoder] = None
        self.wf_gry: Optional[Encoder] = None
        self.wf_grz: Optional[Encoder] = None
        self.wf_rfpha0: Optional[Encoder] = None
        self.wf_rfpha1: Optional[Encoder] = None
        self.wf_rf0: Optional[Encoder] = None
        self.wf_rf1: Optional[Encoder] = None
        self.wf_adc: Optional[Encoder] = None
        self.protocol: Optional[SimulationProtocol] = None

        self.syn: AtomList = AtomList([])
        self.gss: AtomList = AtomList([])
        self.gpe: AtomList = AtomList([])
        self.gro: AtomList = AtomList([])
        self.adc: AtomList = AtomList([])
        self.fre: AtomList = AtomList([])
        self.tx: AtomList = AtomList([])
        self.all_atoms: AtomList = AtomList([])

        if filename is not None:

            # trim trailing _XXX.dsv from filename, if present
            if filename[-4:].lower() == '.dsv':
                filename = filename[:-8]

            self.filename = filename

            from ImporterFactory import ImporterFactory
            # We import ImporterFactory here, since it is dependent on Siemens code, which is not part of the default
            # distribution. If we imported the file at the beginning of this file, it would be impossible to use
            # SequenceTiming.py without the Siemens importer code.
            imp: Importer = ImporterFactory.make_importer(self.filename + '_INF.dsv', self)

            self.tx, self.fre, self.adc, self.gro, self.gpe, self.gss, self.syn, self.all_atoms = imp.get_atoms()

            self.protocol = imp.get_protocol()

            self.wf_grx = imp.gx
            self.wf_gry = imp.gy
            self.wf_grz = imp.gz
            self.wf_rfpha0 = imp.rf_pha0
            self.wf_rfpha1 = imp.rf_pha1
            self.wf_rf0 = imp.rf_ch0
            self.wf_rf1 = imp.rf_ch1
            self.wf_adc = imp.adc

            self.protocol = imp.get_protocol()
        else:
            
            self.all_atoms = AtomList([])
            self.tx = AtomList([])
            self.fre = AtomList([])
            self.adc = AtomList([])
            self.gro = AtomList([])
            self.gpe = AtomList([])
            self.gss = AtomList([])
            self.syn = AtomList([])



        self.rot_matrix: np.ndarray = np.eye(3)  # we use a 1:1 mapping between logical and physical coordinates as the default
        self.active_gradient_table: Optional[List[Tuple[int, int, GradientAtom]]] = None

        self.finding_rotation_matrix_failed = False  # this will be set to True after a failed attempt to find the
        # gradient rotation matrix in order to avoid attempting over and over again.

    def filter_atoms(self, criterion: object) -> AtomList:
        """
        Filter all atoms by criterion. Supports all criteria that AtomList.__getitem__ supports, i.e.:
        - index (not very useful here)
        - slice (not very useful either)
        - string, which will be compared against the name attribute of the atom
        - lambda/function that returns a boolean value

        :param criterion: a criterion by which to filter the atoms, see above for valid types
        :return: an AtomList of all Atom's fulfilling the criterion
        """
        return self.all_atoms[criterion]

    def build_active_gradient_table(self):
        """
        Builds a list of all gradient start and end times. This will be used to identify 'isolated' gradients
        (gradients that are played out on their own, without any simultaneous gradients), which are necessary for
        determining the rotation matrix from scanner coordinates to physical coordinates.
        active_gradient_table is a list of tuples with format (start_time, end_time, GradientAtom). start_time and
        end_time are absolute (i.e., w.r.t. the start of the sequence), and end_time denotes the end of the ramp-down.
        :return: nothing
        """
        grads = self.gro + self.gpe + self.gss

        grads = list(map(lambda x: (x.start_time, x.start_time+x.total_time, x), grads))
        grads.sort(key=lambda x: x[0])  # sort gradients by start time
        self.active_gradient_table = grads

    def get_gradients_at_time(self, t: int) -> AtomList:
        """
        Returns a list of gradients that are active (including ramp-down) at time t
        :param t: the time within the sequence, in microseconds
        :return: a list of active gradients for the given timepoint
        """

        if self.active_gradient_table is None:
            self.build_active_gradient_table()

        # the elements of active_gradients have the format (start_time, end_time, gradient_object)
        active_gradients = list(filter(lambda x: x[0] <= t <= x[1], self.active_gradient_table))

        # return only the gradient objects, not the start and end times.
        return AtomList([x[2] for x in active_gradients])

    def get_atoms_at_time(self, t: float) -> AtomList:

        active_atoms = self.all_atoms[lambda x: x.start_time <= t <= x.start_time + x.total_time]
        active_atoms.sort(key=lambda x: x.start_time)
        return AtomList(active_atoms)

    def calculate_gradient_rotation_matrix(self) -> None:
        """
        Calculate the matrix that translates scanner coordinates (X, Y, Z) into logical coordinates (RO, PE, SS). This
        is achieved by going through the gradient table, identifying 'isolated' gradients (i.e., no other
        gradients are active simultaneously), and then checking the Gx, Gy and Gz amplitudes at the center of the
        gradient in the numerical simulation output.
        Convention from the IDEA manual:

        (Gx)       (PE)
        (Gy) = R * (RO)
        (Gz)       (SS)
        """

        if np.any(self.rot_matrix.flatten() != 0):
            # there is a non-zero element in R, so the calculation has already been performed
            return

        if self.finding_rotation_matrix_failed:  # a previous attempt has failed, so there is no point trying again
            self.rot_matrix = np.zeros((3, 3))  # re-initialize, just to be sure
            return

        # noinspection PyPep8Naming
        rot = np.zeros((3, 3))

        # nested function declaration
        def get_coefficients(g: GradientAtom, index: int) -> bool:
            """
            This function is called to check whether a gradient is isolated, and if so it calculates one column of
            coefficients in the rotation matrix.
            :param g: the isolated gradient
            :param index: the index of the logical gradient axis (0->PE, 1->RO, 2->SS)
            :return: a boolean value indicating whether the coefficients could be calculated from that gradient.
            """
            center_time = g.get_center_time()
            active_gradients = self.get_gradients_at_time(center_time)

            if len(active_gradients) > 1:  # another gradient is active at the same time
                return False
            #  if we reach this point, no other gradient is active
            t = g.get_center_time()

            gx = self.wf_grx.get_value_at_time(t)
            gy = self.wf_gry.get_value_at_time(t)
            gz = self.wf_grz.get_value_at_time(t)

            s = math.sqrt(gx**2 + gy**2 + gz**2)
            sign = g.amplitude/math.fabs(g.amplitude)
            rot[0, index] = gx/s * sign
            rot[1, index] = gy/s * sign
            rot[2, index] = gz/s * sign
            return True

        # first, find a single PE gradient:
        for pe in self.gpe:
            result = get_coefficients(pe, 0)
            if result:
                break
        # RO
        for ro in self.gro:
            result = get_coefficients(ro, 1)
            if result:
                break
        # SS
        for ss in self.gss:
            result = get_coefficients(ss, 2)
            if result:
                break

        # if the rotation matrix has one row with only zeros, it means that there was no single gradient found along
        # that axis. We can fix this by applying the identities
        # X t Y = Z, Y t Z = X, Z t X = Y
        # and calculate the missing row from the existing rows.
        # This is based on the assumption that the rotation matrix is orthogonal, i.e. one row is the cross product
        # of the other two with the correct sign.
        tol = 1e-6
        if np.max(np.abs(rot[:, 0])) < tol:
            rot[:, 0] = np.cross(rot[:, 1], rot[:, 2])
        elif np.max(np.abs(rot[:, 1])) < tol:
            rot[:, 1] = np.cross(rot[:, 2], rot[:, 0])
        elif np.max(np.abs(rot[:, 2])) < tol:
            rot[:, 2] = np.cross(rot[:, 0], rot[:, 1])

        print('Gradient rotation matrix:')
        print(rot)
        print('Determinant of gradient rotation matrix:')
        det = np.linalg.det(rot)
        print(det)
        if math.fabs(det-1) < 1e-6:
            self.rot_matrix = rot
        else:
            print('Determinant is not equal to 1.')
            self.rot_matrix = np.zeros((3, 3))

        #  propagate the rotation matrix to all gradient objects
        for x in self.gpe + self.gro + self.gss:
            x.set_rotation_matrix(self.rot_matrix)

        print('Slice orientation:')
        v_normal = np.dot(self.rot_matrix, np.asarray([0, 0, 1]))    # transform the slice-select direction
        # (slice normal) to scanner coordinates

        print(v_normal)
        alpha_sagittal = math.acos(v_normal[0]) * 360/2/pi
        alpha_coronal = math.acos(v_normal[1]) * 360/2/pi
        alpha_transversal = 180-math.acos(v_normal[2]) * 360/2/pi

        print('Angle between slice normal and:')
        print('sagittal: %.2f°' % alpha_sagittal)
        print('coronal: %.2f°' % alpha_coronal)
        print('transversal: %.2f°' % alpha_transversal)

    # noinspection PyPep8Naming
    def get_gradient_waveforms_PRS(self, g: GradientAtom) -> Tuple[np.ndarray, np.ndarray]:
        """
        Get the gradient waveforms on the PE, RO and SS axes for the duration of gradient g
        Note that the waveform is shown incorrectly if two gradients overlap on the same axis.
        :param g: gradient atom
        :return: a tuple with a time vector (1-dim ndarray) and a waveform of identical size of the waveform along
        the direction of g
        """

        t0 = g.start_time
        t1 = g.start_time + g.total_time

        t, gx = self.wf_grx.get_interval(t0, t1)
        _, gy = self.wf_gry.get_interval(t0, t1)
        _, gz = self.wf_grz.get_interval(t0, t1)

        prs = np.zeros((3, t.size))

        self.calculate_gradient_rotation_matrix()  # make sure that the rotation matrix is calculated
        r_inv = np.linalg.inv(self.rot_matrix)

        for i in range(0, t.size):
            vec = np.asarray([gx[i], gy[i], gz[i]])
            prs[:, i] = np.dot(r_inv, vec)  # dot represents multiplication between matrix and vector

        if g.axis == AtomType.GP:
            print('Axis: PE')
            values = prs[0, :]
        elif g.axis == AtomType.GR:
            print('Axis: RO')
            values = prs[1, :]
        else:
            print('Axis: SS')
            values = prs[2, :]
        return t, values

    def get_gradient_rotation_matrix(self) -> np.ndarray:
        self.calculate_gradient_rotation_matrix()
        return self.rot_matrix

    def get_sequence_duration(self) -> int:
        """
        Get the duration of the sequence in us
        :return: the duration of the sequence, in us
        """

        duration = 0

        for x in self.all_atoms:
            if x.start_time + x.total_time > duration:
                duration = x.start_time + x.total_time

        return duration

    def print_trigger_list(self) -> None:
        """
        Print a list of all trigger (sync) atoms found, ordered by start time.
        :return: Nothing
        """
        triggers_sorted = sorted(self.syn, key=lambda x: x.start_time)

        print(f"{'index'}\t{'Start time [µs]':>15}\t{'duration [µs]':>13}\t{'name'}")
        print('-' * 50)
        ind = 0
        for tr in triggers_sorted:
            print(f"{ind:5}\t{tr.start_time:15}\t{tr.duration:13}\t{tr.name}")
            ind += 1

    def read_waveforms(self) -> None:
        """
        Read the waveforms for all waveform managers
        :return: nothing
        """
        self.wf_grx.encoder_from_file()
        self.wf_gry.encoder_from_file()
        self.wf_grz.encoder_from_file()
        self.wf_rf0.encoder_from_file()
        self.wf_adc.encoder_from_file()
        self.wf_rfpha0.encoder_from_file()
        self.wf_rf1.encoder_from_file()

    def waveforms_from_list(self) -> None:
        self.wf_rf0 = self.tx[0].waveform0
        self.wf_rf1 = self.tx[0].waveform1
        self.wf_rfpha0 = self.tx[0].phase0
        self.wf_rfpha1 = self.tx[0].phase1
        for j in range(1, len(self.tx)):
            self.wf_rf0 = self.wf_rf0 + self.tx[j].waveform0
            self.wf_rfpha0 = self.wf_rfpha0 + self.tx[j].phase0
            self.wf_rf1 = self.wf_rf1 + self.tx[j].waveform1
            self.wf_rfpha1 = self.wf_rfpha1 + self.tx[j].phase1

        self.wf_grz = self.__sum_encoders(self.gss)
        self.wf_gry = self.__sum_encoders(self.gpe)
        self.wf_grx = self.__sum_encoders(self.gro)

        self.wf_adc = self.__sum_encoders(self.adc)

    def get_simu_params(self):
        """
        Return a dict with the simulation parameters for MRiLab
        :return: a dict with simulation parameters
        """
        return self.protocol.generate_simu_dict()

    @staticmethod
    def is_overlapping(atom: AtomBase, other_atoms: AtomList) -> bool:
        """
        Returns true if atom overlaps with one of the elements of other_atoms, i.e., if one of other_atoms is active
        while atom is active.
        :param atom: an atom
        :param other_atoms: a list of atoms
        :return: True if there is an overlap, false otherwise
        """
        start = atom.start_time
        end = start + atom.total_time

        for a in other_atoms:
            if a is atom:  # in this case, a and atom would overlap naturally
                continue

            a_start = a.start_time
            a_end = a_start + a.total_time

            if start <= a_start <= end:
                return True
            if start <= a_end <= end:
                return True
        return False

    @staticmethod
    def __sum_encoders(li: List[AtomBase]) -> 'LineEncoder':
        if li is None or len(li) == 0:
            return LineEncoder()
        enc = li[0].get_encoder()
        for j in range(1, len(li)):
            enc = enc + li[j].get_encoder()
        return enc

if __name__ == '__main__':
    from SequenceTiming import SequenceTiming
    from MRiLabExporter import  MRiLabExporter
    from SequenceDiagram import SequenceDiagram
    import matplotlib.pyplot as plt

    import os

    sim_file = os.path.expanduser('~/tmp/PSD_EPITest_1sl_10mm/SimulationProtocol_INF.dsv')
    st = SequenceTiming(sim_file)

    exp = MRiLabExporter(st, ref_voltage=100)

    out_dir = os.path.expanduser('~/tmp/PSD_EPITest_1sl_10mm')
    exp.write_mrilab_xml(out_dir, 'PSD_EPITest_1sl_10mm.xml')
