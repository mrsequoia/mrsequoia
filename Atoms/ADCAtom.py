"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from typing import Optional

from .AtomBase import AtomBase
from .FreqPhaseAtom import FreqPhaseAtom


class ADCAtom(AtomBase):
    """
    ADCAtom: a representation of a single ADC (typically, one line in k-space), including all of the associated MDH
    entries.
    """

    def __init__(self, start_time: int, name: str, duration: float, num_adcs: int, timing):
        """
        The constructor for an ADC Atom
        :param start_time: he absolute start time (relative to the start of the sequence), in µs
        :param name: the name/identifier of the atom
        :param duration: the ADC duration, in µs
        :param num_adcs: the number of points sampled
        :param timing: a reference to the parent SequenceTimining object to query global information from, e.g., the
        gradient rotation matrix
        """
        super().__init__(start_time, name, duration, timing)
        self.num_adcs: int = num_adcs
        self.adc_info: Optional['ADCInfo'] = None
        self.offset_phase: float = 0.0
        self.dynamic_phase: float = 0.0

    def add_adc_info(self, ai: 'ADCInfo') -> None:
        """
        Attach an ADCInfo object (containing MDH entries) to the ADC
        :param ai: an ADCInfo object
        :return: nothing
        """
        self.adc_info = ai

    def has_adc_info(self) -> bool:
        """
        Find out if this object has an ADCInfo object associated with it
        :return: a boolean value
        """
        return self.adc_info is not None

    def get_freq_phase_atom(self) -> FreqPhaseAtom:

        return FreqPhaseAtom(self.start_time, 'freq_phase_'+self.name, 0, self.offset_phase+self.dynamic_phase, self.timing)

    def to_mrilab_xml(self) -> str:
        """
        Return an XML representation of the ADC object that can be imported into MRiLab
        :return: the XML representation as a string
        """

        # example:
        # <ADCBlock DupSpacing="0" Duplicates="1" Notes="blah" Switch="$1'on','off'"
        #                    sSample="VCtl.ResFreq"
        #                    tEnd="VCtl.TE+20e-3"
        #                    tStart="VCtl.TE-20e-3"/>

        t_start = self.start_time * 1e-6
        t_end = (self.start_time + self.duration) * 1e-6

        xml = f'''<ADCBlock DupSpacing="0" Duplicates="1" Notes="{self.name}" Switch="$1'on','off'"
                   sSample="VCtl.ResFreq"
                   tEnd="{t_end:.9f}"
                   tStart="{t_start:.9f}"/>
        '''
        return xml

    def __str__(self) -> str:
        """
        Generate a string representation of the atom
        :return: a string representation
        """
        return "ADC with name %s, start_time %d, duration %.1f, num_adc %d" % (self.name, self.start_time,
                                                                               self.duration, self.num_adcs)
