"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import math
from typing import TypeVar, Union
from LineEncoder import LineEncoder
from SeqTree.ParameterizedFunction import ParameterizedFunction

import numpy as np

# SequenceTiming cannot be imported as it would cause a circular import situation, so for the sake of static type
# checking we forward-declare it here by creating an alias.
SeqTim = TypeVar('SeqTim', bound='SequenceTiming')

# a data type that holds either a float value or a ParameterizedFunction that can be evaluated at runtime
ValueType = Union[float, ParameterizedFunction]


class AtomBase:
    """
    AtomBase is the base class for all atoms, which represent a single instance
    of a gradient, RF pulse, ADC, sync signal or frequency/phase adjustment. This class serves as the
    base class containing only the functionality common to all atom types. Specialized functionality
    is implemented in the derived classes. This class is intended to be an abstract base class, and
    it should not be instantiated; use a derived class of the correct type instead.
    """

    def __init__(self, start_time: int, name: str, duration: float, timing: SeqTim):
        """
        The base constructor that sets the attributes that are common to all types of atoms.
        :param start_time: the start time (in µs), relative to the start of the sequence (i.e., absolute)
        :param name: the name/identifier of the atom. This can be used for filtering atoms by name
        :param duration: the duration of the atom, in µs. This follows the IDEA convention that the duration of a
        gradient is ramp-up time + flattop time, excluding the ramp-down. For all other atom types, the duration is
        what one would expect.
        :param timing: a reference to the parent SequenceTimining object to query global information from, e.g., the
        gradient rotation matrix
        """
        self.start_time: int = start_time
        self.duration: float = duration  # ADCs can have a non-integer duration, so we need to use float here
        self.name: str = name
        self.timing: SeqTim = timing
        self.total_time: float = duration  # for gradients, we will overwrite this with duration + ramp down time

    def to_html(self, additional_information: str = '') -> str:

        text = '''
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        '''
        return text % (str(self.__class__.__name__), self.name, f"{self.start_time:,.1f}", f"{self.total_time:,.1f}",
                       additional_information)
        # f strings to use comma as the separator for 1000's

    def get_encoder(self) -> LineEncoder:
        return LineEncoder()

    def set_start_time(self, start_time: float) -> None:
        self.start_time = start_time

##########################################################
# some helper functions outside of any class definition: #
##########################################################


def unwrap(data: np.ndarray) -> np.ndarray:
    """
    Unwrap the data using the simplest one-dimensional algorithm (Itoh's method):
    Traverse through the data, if the difference between two points is larger than PI, add multiples of 2 PI to the
    second point such that the difference falls in the range (-PI, PI).
    :param data: a one-dimensional array of wrapped phase data
    :return: the (hopefully) unwrapped version of the data
    """
    values = np.empty(data.shape)
    values[0] = data[0]
    for i in range(1, data.size):
        diff = data[i]-values[i-1]
        if math.fabs(diff) > math.pi:
            offset = round(diff/(2*math.pi)) * 2*math.pi
            values[i] = data[i]-offset
        else:
            values[i] = data[i]
    return values
