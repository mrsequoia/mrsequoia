"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import math

import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import spectrogram

from SequenceTiming import SequenceTiming


class Spectrogram:
    """
    A class for plotting acoustic vibration spectrograms and spectra.
    """

    def __init__(self, st: SequenceTiming):
        """
        The constructor. How creative.
        :param st: a SequenceTiming object
        """
        super().__init__()
        self.timing = st

    def plot_spectra(self, t0: int = None, t1: int = None, fmax: float = 2000, dt: float = 1e-5) -> None:
        """
        Plot the acoustic frequency spectra.
        :param t0: start time (in µs). If omitted, the start of the sequence is used.
        :param t1: end time (in µs). If omitted, the end of the sequence is used.
        :param fmax: The maximum mechanical frequency of interest (in Hz). Defaults to 2000 Hz
        :param dt: the sampling dwell time (time per data point)
        :return: nothing
        """

        def __plot(data: np.ndarray, sampling_interval: float, max_freq: float, title: str) -> None:
            """
            Plot spectrogram and frequency spectrum for one physical axis side by side.
            :param data: the array containing the waveform
            :param sampling_interval: the time between two sample points
            :param max_freq: maximum frequency of interest
            :param title: title of the figure
            :return: nothing
            """
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 6))
            fig.suptitle(title, fontsize=16)
            plt.subplots_adjust(left=0.1, bottom=None, right=None, top=None, wspace=0.2, hspace=0.1)
            self.plot_spectrogram(data, sampling_interval, max_freq, ax1)
            self.plot_fft(data, sampling_interval, max_freq, ax2)
            return ax1, ax2

        _, wf_x = self.timing.wf_grx.expand_fully(t0, t1)
        _, fft_gx = __plot(wf_x, dt, fmax, 'X gradient')

        _, wf_y = self.timing.wf_gry.expand_fully(t0, t1)
        _, fft_gy = __plot(wf_y, dt, fmax, 'Y gradient')

        _, wf_z = self.timing.wf_grz.expand_fully(t0, t1)
        _, fft_gz = __plot(wf_z, dt, fmax, 'Z gradient')

        # set the same y range for all fft plots:
        maxy = np.max([fft_gx.get_ylim()[1], fft_gy.get_ylim()[1], fft_gz.get_ylim()[1]])
        [ax.set_ylim([0, maxy]) for ax in [fft_gx, fft_gy, fft_gz]]

        # set xrange
        [ax.set_xlim([0, fmax]) for ax in [fft_gx, fft_gy, fft_gz]]

    def downsample(self, data: np.ndarray, dt: float, fmax: float) -> (np.ndarray, int):
        """
        downsample the timeseries data, which has a temporal resolution of dt, to the lowest temporal resolution that
        still allows for resolving frequency fmax according to the Nyquist criterion.
        :param data: one-dimensional time series data
        :param dt: temporal resolution of data (in seconds)
        :param fmax: max. desired frequency (in Hz)
        :return: a tuple (downsampled version of data, downsampling_factor)
        """

        # Nyquist: the highest resolvable frequency is half the sampling rate
        f = 1.0 / (2*dt)

        if f < fmax:
            print('Error: fmax has to be <= 1/(2*dt) according to Nyquist!')
            return data

        ds_factor = math.floor(f / fmax)
        ind = range(0, data.size, ds_factor)
        return data[ind], ds_factor

    def plot_spectrogram(self, data: np.ndarray, dt: float, fmax: float, ax=None) -> None:
        """
        Plot the spectrogram (time vs frequency) with frequency along the horizontal axis to match the spectrum plot
        :param data: the waveform data (1D array)
        :param dt: the dwell time (sampling time per point)
        :param fmax: the maximum acoustic frequency of interest
        :param ax: optional axes object to plot into, by default, the current axes are used (or generated)
        :return: nothing
        """

        data_ds, ds_factor = self.downsample(data, dt, fmax)

        ax = ax or plt.gca()

        f, t, spec = spectrogram(data_ds, 1 / dt / ds_factor)

        cmap = plt.get_cmap('magma')  # define a color map
        #plt.subplot(ax)
        plt.axes(ax)
        ax.pcolormesh(f, t, spec.transpose(), cmap=cmap)
        plt.xlabel('Frequency [Hz]', fontsize=12)
        plt.ylabel('Time [sec]', fontsize=12)

    def plot_fft(self, data: np.ndarray, dt: float, fmax: float, ax=None):
        """
        Plot the FFT-based right-handed frequency spectrum for the entire waveform (i.e., non-windowed)
        :param data: 1D waveform array
        :param dt: sampling interval
        :param fmax: maximum acoustic frequency of interest
        :param ax: optional axes object to plot into, by default, the current axes are used (or generated)
        :return: nothing
        """

        data_ds, ds_factor = self.downsample(data, dt, fmax)
        ax = ax or plt.gca()

        ff = np.fft.rfft(data_ds)

        nfreqs = data_ds.size/2 + 1 if data_ds.size % 2 == 0 else (data_ds.size+1)/2

        freqs = np.asarray(range(0, int(nfreqs))) * 1/(ds_factor*dt) / (2 * nfreqs)

        #plt.subplot(ax)
        plt.axes(ax)
        ax.plot(freqs, np.abs(ff))

        plt.xlabel('Frequency [Hz]', fontsize=12)
        plt.ylabel('Amplitude', fontsize=12)
