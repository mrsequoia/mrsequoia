# MRSequoia
[![](MRSequoiaLogo_500px.png)](MRSequoiaLogo.png)
 
### A framework for designing, prototyping, testing and validating MR sequences
*Author: Sebastian Hirsch*

MRSequoia provides MR sequence developers with an easily extensible framework for prototyping new and testing existing MR sequences. The following list highlights some of its capabilities:

## Highlights
### Generate sequence timing:
- Define sequence timing using the included SequenceBuilder (see [SequenceBuilder.md](documentation/SequenceBuilder.md)).
[![](documentation/Gradient_Echo_Tree_View.png)](documentation/Gradient_Echo_Tree_View.png)
- Import the output of the Siemens IDEA sequence simulator (see [SequenceImport.md](documentation/SequenceImport.md)).

### Sequence visualization:
- Use the built-in sequence visualizer (SequenceDiagram) to obtain a zoomable view of the sequence timing, and inspect the properties of RF pulses, gradients, and ADCs.
[![](documentation/Gradient_Echo_Sequence_Diagram.png)](documentation/Gradient_Echo_Sequence_Diagram.png)
### Test sequence timing:
- Use a set of pre-existing tests to check various aspects of the sequence timing, e.g., that TR is correct.
- Define your own tests for testing additional properties.

See [SequenceTesting](documentation/SequenceTesting.md) for details.

### MR physics simulation:
- Export the MR sequence as an XML file that can be imported into the open-source MR physics simulator [MRiLab][MRiLab] ([Liu et al. 2016](https://ieeexplore.ieee.org/document/7676360)) to investigate what an image acquired with that sequence would look like. (Note: At the moment, this works well with sequences defined using SequenceBuilder. For sequences imported from Siemens IDEA, this is work in progress.) See [MRiLab.md](documentation/MRiLab.md) for instructions.

[![](documentation/MRiLab_Gradient_Echo_Example.png)](documentation/MRiLab_Gradient_Echo_Example.png)

### Docker support

- MRSequoia comes with shell scripts to build and run inside a docker container, thus avoiding the need for installation of JupyterLab. See [Docker.md](documentation/Docker.md)

## General information
MRSequoia is entirely written in the [Python®][Python] programming language, using established modules such as [SciPy][SciPy], [NumPy][NumPy], [Jupyter][jupyter], and [matplotlib][matplotlib], and is thus easily extensible. As of now, sequences can only be composed within MRSequioa, or imported from Siemens IDEA simulation output (requiring installation of the importer files from the Siemens IDEA forum, see below), but other MR scanner vendors can be supported as well in the future (you're welcome to get in touch if you would like to help with that!). The sequence timing is converted to a sequence of atoms (GradientAtoms, TXAtoms, ADCAtoms, etc), in analogy to how atoms are assembled in molecules.
 
MRSequoia is developed and maintained by Sebastian Hirsch at the Berlin Center for Advanced Neuroimaging at Charité - Universitätsmedizin Berlin. Its core components are licensed under a 3-clause BSD license (see [LICENSE.txt](LICENSE.txt)) and thus freely available and can be downloaded and modified by anyone with the exception of the Siemens-specific part of the code, which is available at [https://www.magnetom.net/](https://www.magnetom.net/t/introducing-mrsequoia/3682/5), subject to the existence of a Master Research Agreement between your institute and Siemens Healthineers. 

These examples are also included in `examples_commandline.py` and `JupyterLab_example.ipynb`

Instructions for installing MRSequoia can be found in [Installation.md](documentation/Installation.md)


[//]: # (Collect footnotes below)

[Python]: https://www.python.org/

[SciPy]: https://www.scipy.org/

[NumPy]: https://www.numpy.org/

[matplotlib]: https://matplotlib.org/

[MRiLab]: http://mrilab.sourceforge.net

[Jupyter]: https://jupyter.org

## References
* F. Liu, J.V. Velikina, W.F. Block, R. Kijowski, A.A. Samsonov. Fast Realistic MRI Simulations Based on Generalized Multi-Pool Exchange Tissue Model. IEEE Transactions on Medical Imaging. 2016. doi: 10.1109/TMI.2016.2620961. https://ieeexplore.ieee.org/document/7676360

## Acknowledgements
Max Müller (University Hospital Erlangen) provided valuable feedback, bug reports and suggestions for improvements.                       

## License & Disclaimer:
MRSequoia is released under the Modified BSD License ("3-clause BSD") 

Copyright (c) 2020, Sebastian Hirsch

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
      
   * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
      
   * Neither the names of the MRSequoia developers nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SEBASTIAN HIRSCH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Author:
MRSequoia was written by

    Sebastian Hirsch
    Berlin Center for Advanced Neuroimaging (BCAN)
    Charité - Universitätsmedizin Berlin
    &
    Berlin Center for Computational Neuroscience
    Contact: sebastian.hirsch<insert "at" character here>bccn-berlin.de

## Code Coverage:
[![codecov](https://codecov.io/gl/sebastiancervus/mrsequoia/branch/main/graph/badge.svg?token=8XSNGJ1OU8)](https://codecov.io/gl/sebastiancervus/mrsequoia)